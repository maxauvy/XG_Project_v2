<?php

$lang['Version']     = 'Version';
$lang['Description'] = 'Description';
$lang['changelog']   = array(

'2.11.0' => 'Sin fecha
- [Cambios]
- Se cambian las imágenes.-
- Se añade un nuevo tema por defecto, el de XG Project, que será 100% libre.-
',

'2.10.10' => '21/11/2014
- [Cambios]
- Se cambian las referencias a la nueva web en xgproject.org.-

- [Bugs]
- Se soluciona un error en el check de la actualización.-
- Se mejora la carga de la página de administración.-
- Pequeñas mejoras en lenguaje.-
',

'2.10.9' => '06/10/2014

Se comienza el desarrollo basado en XG Proyect 2.10.8

- [Cambios]
- El proyecto cambia de nombre a XG Project y comienza de nuevo el desarrollo activo.-

- [Mejoras]
- Se corrigen pequeños textos de lenguaje.-
');
<?php

//SERVER GENERALS
$lang['Metal']								= 'Métal';
$lang['Crystal']							= 'Cristal';
$lang['Deuterium']							= 'Deutérium';
$lang['Darkmatter']							= 'Anti-matière';
$lang['Energy']								= 'Énergie';
$lang['Messages']							= 'Messages';
$lang['write_message']						= 'Écrire un message';

$lang['type_mission'][1]  					= 'Attaquer';
$lang['type_mission'][2]  					= 'Attaque groupée';
$lang['type_mission'][3]  					= 'Transporter';
$lang['type_mission'][4]  					= 'Stationner';
$lang['type_mission'][5]  					= 'Stationner chez un allié';
$lang['type_mission'][6]  					= 'Espionner';
$lang['type_mission'][7]  					= 'Coloniser';
$lang['type_mission'][8]  					= 'Recycler';
$lang['type_mission'][9]  					= 'Détruire';
$lang['type_mission'][15] 					= 'Expéditions';

$lang['user_level'] = array (
	'0' => 'Joueur',
	'1' => 'Modérateur',
	'2' => 'Opérateur',
	'3' => 'Administrateur',
);

// GAME.PHP
$lang['see_you_soon']						= 'À bientôt !';
$lang['page_doesnt_exist']					= 'La page que vous avez demandée n\'existe pas';

//SHORT NAMES FOR COMBAT REPORTS
$lang['tech_rc'] = array (
202 => "P.transp.",
203 => "G.transp.",
204 => "Ch.léger",
205 => "Ch.lourd",
206 => "Croiseur",
207 => "V.bataille",
208 => "V.colo",
209 => "Recycleur",
210 => "Sonde",
211 => "Bombardier",
212 => "Sat.sol",
213 => "Destr.",
214 => "Rip",
215 => "Traqueur",

401 => "Missile",
402 => "L.léger.",
403 => "L.lourd",
404 => "Can.Gauss",
405 => "Art.ions",
406 => "Lanc.plasma",
407 => "P.bouclier",
408 => "G.bouclier",
);

//----------------------------------------------------------------------------//
//TOPNAV
$lang['tn_vacation_mode']					= 'Mode vacances activé jusqu\'au ';
$lang['tn_vacation_mode_active'] 				= "Mode vacances actif"; 
$lang['tn_delete_mode']						= 'Votre compte est en mode effacement. Il sera supprimé le ';

//----------------------------------------------------------------------------//
//LEFT MENU
$lang['lm_overview']							= 'Vue générale';
$lang['lm_galaxy']							= 'Galaxie';
$lang['lm_empire']							= 'Empire';
$lang['lm_fleet']							= 'Flotte';
$lang['lm_buildings']						= 'Bâtiments';
$lang['lm_research']						= 'Laboratoire';
$lang['lm_shipshard']						= 'Chantier spatial';
$lang['lm_defenses']						= 'Défense';
$lang['lm_resources']						= 'Ressources';
$lang['lm_officiers']						= 'Officiers';
$lang['lm_trader']						= 'Marchand';
$lang['lm_technology']						= 'Technologie';
$lang['lm_messages']						= 'Messages';
$lang['lm_alliance']						= 'Alliance';
$lang['lm_buddylist']						= 'Liste d\'amis';
$lang['lm_rules']							= 'Règles';
$lang['lm_notes']						= 'Notes';
$lang['lm_statistics']						= 'Statistiques';
$lang['lm_search']						= 'Recherche';
$lang['lm_options']						= 'Options';
$lang['lm_banned']						= 'Piloris';
$lang['lm_contact']						= 'Contact';
$lang['lm_forums']						= 'Forum';
$lang['lm_logout']						= 'Déconnexion';
$lang['lm_administration']					= 'Administration';
$lang['lm_game_speed']						= 'Jeu';
$lang['lm_fleet_speed']						= 'Flottes';
$lang['lm_resources_speed']					= 'Ressources';
$lang['lm_queues']						= 'Queues';
$lang['lm_bugreport']						= 'Rapport de bug';

//----------------------------------------------------------------------------//
//OVERVIEW
$lang['ov_newname_error']					= 'Vous avez entré un caractère invalide. Vous ne pouvez entrer seulement des caractères alphanumériques.';
$lang['ov_planet_abandoned']				= 'La colonie a été abandonnée !';
$lang['ov_principal_planet_cant_abanone']	= 'La planète mère ne peut être abandonnée !';
$lang['ov_wrong_pass']						= 'Mot de passe incorrect, saisissez le à nouveau.';
$lang['ov_have_new_message']				= 'Vous avez 1 nouveau message';
$lang['ov_have_new_messages']				= 'Vous avez %m nouveaux messages';
$lang['ov_free']							= 'Aucune construction en cours';
$lang['ov_news']							= 'Nouveautés';
$lang['ov_place']							= 'Rang';
$lang['ov_of']								= 'sur';
$lang['ov_planet']							= 'Planète';
$lang['ov_server_time']						= 'Heure du serveur ';
$lang['ov_events']							= 'Évènements';
$lang['ov_diameter']						= 'Diamètre';
$lang['ov_distance_unit']					= 'km';
$lang['ov_temperature']						= 'Température';
$lang['ov_aprox']							= 'approx.';
$lang['ov_temp_unit']						= '°C';
$lang['ov_to']								= 'à';
$lang['ov_position']						= 'Position';
$lang['ov_points']							= 'Points';
$lang['ov_security_request']				= 'Requête de sécurité';
$lang['ov_security_confirm']				= 'Veuillez confirmer la suppression de la planète.';
$lang['ov_with_pass']						= 'en entrant votre mot de passe';
$lang['ov_password']						= 'Mot de passe';
$lang['ov_delete_planet']					= 'Supprimer la planète !';
$lang['ov_your_planet']						= 'Votre planète';
$lang['ov_coords']							= 'Position';
$lang['ov_abandon_planet']					= 'abandonner colonie';
$lang['ov_planet_name']						= 'Nom';
$lang['ov_actions']							= 'Fonctions';
$lang['ov_planet_rename']					= 'renommer';
$lang['ov_planet_rename_action']			= 'renommer';
$lang['ov_abandon_planet_not_possible']		= 'Il n\'est pas possible d\'abandonner une planète où les flottes sont en mouvement';
$lang['ov_fields']							= 'cases';
$lang['ov_online_members']					= 'Joueurs en ligne';
$lang['ov_pts_build']						= 'Bâtiments';
$lang['ov_pts_fleet']						= 'Flotte';
$lang['ov_pts_reche']						= 'Recherche';
$lang['ov_pts_defen']						= 'Défenses';

//----------------------------------------------------------------------------//
//GALAXY
$lang['gl_no_deuterium_to_view_galaxy']		= 'Vous n\'avez pas assez de deutérium !';
$lang['gl_legend']							= 'Légende';
$lang['gl_strong_player']					= 'joueur fort';
$lang['gl_week_player']						= 'joueur faible (débutant)';
$lang['gl_vacation']						= 'Mode Vacances';
$lang['gl_banned']							= 'banni';
$lang['gl_inactive_seven']					= 'Inactif 7+';
$lang['gl_inactive_twentyeight']			= 'Inactif 28+';
$lang['gl_s']								= 'f';
$lang['gl_w']								= 'n';
$lang['gl_v']								= 'v';
$lang['gl_b']								= 'b';
$lang['gl_i']								= 'i';
$lang['gl_I']								= 'I';
$lang['gl_populed_planets']					= 'planète(s) colonisée(s)';
$lang['gl_out_space']						= 'Espace infini';
$lang['gl_avaible_missiles']				= 'missiles disponibles';
$lang['gl_fleets']							= 'flottes';
$lang['gl_avaible_recyclers']				= 'recycleurs disponibles';
$lang['gl_avaible_spyprobes']				= 'sondes disponibles';
$lang['gl_missil_launch']					= 'Lancement de missile';
$lang['gl_missil_to_launch']				= 'Nombre de missiles (reste(nt) <b>%d</b>):';
$lang['gl_all_defenses']					= 'Tout';
$lang['gl_objective']						= 'Objectif';
$lang['gl_missil_launch_action']			= 'OK';
$lang['gl_galaxy']							= 'Galaxie';
$lang['gl_solar_system']					= 'Système solaire';
$lang['gl_show']							= 'Afficher';
$lang['gl_pos']								= 'Pos';
$lang['gl_planet']							= 'Planète';
$lang['gl_name_activity']					= 'Nom (activité)';
$lang['gl_moon']							= 'Lune';
$lang['gl_debris']							= 'Débris';
$lang['gl_player_estate']					= 'Joueur (statut)';
$lang['gl_alliance']						= 'Alliance';
$lang['gl_actions']							= 'Actions';
$lang['gl_spy']								= 'Espionnage';
$lang['gl_buddy_request']					= 'Demander en ami';
$lang['gl_missile_attack']					= 'Attaque de missiles';
$lang['gl_with']							= ' constituée de ';
$lang['gl_member']							= ' membre';
$lang['gl_member_add']						= '(s)';
$lang['gl_alliance_page']					= 'Page de l alliance';
$lang['gl_see_on_stats']					= 'Statistiques';
$lang['gl_alliance_web_page']				= 'Site de l alliance';
$lang['gl_debris_field']					= 'Champs de débris';
$lang['gl_collect']							= 'Collecter';
$lang['gl_resources']						= 'Ressources';
$lang['gl_features']						= 'Fonctions';
$lang['gl_diameter']						= 'Diamètre';
$lang['gl_temperature']						= 'Température';
$lang['gl_phalanx']							= 'Phalange';
$lang['gl_planet_destroyed']				= 'Planète détruite';
$lang['gl_player']							= '';
$lang['gl_in_the_rank']						= ' classé ';
$lang['gl_stat']							= 'Statistiques';

//----------------------------------------------------------------------------//
//PHALANX
$lang['px_no_deuterium']					= 'Vous n\'avez pas assez de deutérium !';
$lang['px_scan_position']					= 'Scanner position';
$lang['px_fleet_movement']					= 'Mouvement actuel de la flotte';

//----------------------------------------------------------------------------//
//IMPERIUM
$lang['iv_imperium_title']					= 'Vision de l\'empire';
$lang['iv_planet']							= 'Planète';
$lang['iv_name']							= 'Nom';
$lang['iv_coords']							= 'Coordonnées';
$lang['iv_fields']							= 'Cases';
$lang['iv_resources']						= 'Ressources';
$lang['iv_buildings']						= 'Bâtiments';
$lang['iv_technology']						= 'Technologies';
$lang['iv_ships']							= 'Vaisseaux';
$lang['iv_defenses']						= 'Défenses';

//----------------------------------------------------------------------------//
//FLEET - FLEET1 - FLEET2 - FLEET3 - FLEETACS - FLEETSHORTCUTS
$lang['fl_returning']						= 'Retour';
$lang['fl_onway']							= 'En route';
$lang['fl_r']								= '(R)';
$lang['fl_a']								= '(A)';
$lang['fl_send_back']						= 'Retour';
$lang['fl_acs']								= 'Associer';
$lang['fl_no_more_slots']					= 'Tous les slots de flottes sont utilisés !';
$lang['fl_speed_title']						= 'Vitesse: ';
$lang['fl_continue']						= 'Continuer';
$lang['fl_no_ships']						= 'Il n\'y a pas de vaisseaux disponibles';
$lang['fl_remove_all_ships']				= 'Aucun vaisseau';
$lang['fl_select_all_ships']				= 'Tous les vaisseaux';
$lang['fl_fleets']							= 'Flottes';
$lang['fl_expeditions']						= 'Expéditions';
$lang['fl_number']							= 'N°';
$lang['fl_mission']							= 'Mission';
$lang['fl_ammount']							= 'Vaisseaux (total)';
$lang['fl_beginning']						= 'Départ';
$lang['fl_departure']						= 'Heure de départ';
$lang['fl_destiny']							= 'Destination';
$lang['fl_objective']						= 'Heure d\'arrivée';
$lang['fl_arrival']							= 'Temps restant';
$lang['fl_order']							= 'Ordre';
$lang['fl_new_mission_title']				= 'Nouvelle mission : choisissez les vaisseaux';
$lang['fl_ship_type']						= 'Type de vaisseau';
$lang['fl_ship_available']					= 'Disponible';
$lang['fl_planet']							= 'Planète';
$lang['fl_debris']							= 'Débris';
$lang['fl_moon']							= 'Lune';
$lang['fl_planet_shortcut']					= '(P)';
$lang['fl_debris_shortcut']					= '(D)';
$lang['fl_moon_shortcut']					= '(L)';
$lang['fl_no_shortcuts']					= 'Pas de raccourcis';
$lang['fl_anonymous']						= 'Anonyme';
$lang['fl_shortcut_add_title']				= 'Nom [Galaxie / Système / Planète]';
$lang['fl_shortcut_name']					= 'Nom';
$lang['fl_shortcut_galaxy']					= 'Galaxie';
$lang['fl_shortcut_solar_system']			= 'Système solaire';
$lang['fl_clean']							= 'Supprimer';
$lang['fl_register_shorcut']				= 'Enregistrer';
$lang['fl_shortcuts']						= 'Raccourcis';
$lang['fl_reset_shortcut']					= 'Remettre à zéro';
$lang['fl_dlte_shortcut']					= 'Supprimer';
$lang['fl_back']							= 'Retour';
$lang['fl_shortcut_add']					= 'Ajouter';
$lang['fl_shortcut_edition']				= 'Éditer: ';
$lang['fl_no_colony']						= 'Pas de colonies';
$lang['fl_send_fleet']						= 'Envoi de flotte';
$lang['fl_fleet_speed']						= 'Vitesse';
$lang['fl_distance']						= 'Distance';
$lang['fl_flying_time']						= 'Durée du trajet';
$lang['fl_fuel_consumption']				= 'Consommation de carburant';
$lang['fl_max_speed']						= 'Vitesse max.';
$lang['fl_cargo_capacity']					= 'Capacité de chargement';
$lang['fl_shortcut']						= 'Raccourcis';
$lang['fl_shortcut_add_edit']				= '(Ajouter / Éditer)';
$lang['fl_my_planets']						= 'Mes planètes';
$lang['fl_acs_title']						= 'Attaque groupée';
$lang['fl_hold_time']						= 'Temps de stationnement';
$lang['fl_resources']						= 'Ressources';
$lang['fl_max']								= 'max';
$lang['fl_resources_left']					= 'Ressources restantes';
$lang['fl_all_resources']					= 'Toutes les ressources';
$lang['fl_expedition_alert_message']		= 'Attention! Les expéditions sont des missions risquées, vous pouvez perdre vos flottes !';
$lang['fl_vacation_mode_active']			= 'Mode vacances activé';
$lang['fl_expedition_tech_required']		= 'Vous ne possédez pas la technologie Expéditions !';
$lang['fl_expedition_fleets_limit']			= 'Vous ne pouvez pas envoyer plus de flottes en Expédition !';
$lang['fl_week_player']						= 'Le joueur est trop faible !';
$lang['fl_strong_player']					= 'Le joueur est trop fort !';
$lang['fl_in_vacation_player']				= 'Ce joueur est en mode vacances !';
$lang['fl_no_slots']						= 'Pas d\'autres slots disponibles';
$lang['fl_empty_transport']					= 'Vous ne pouvez transporter aucune ressource.';
$lang['fl_planet_populed']					= 'Cette planète est déjà occupée !';
$lang['fl_stay_not_on_enemy']				= 'Vous ne pouvez pas stationner sur les planètes ennemies !';
$lang['fl_not_ally_deposit']				= 'Pas de dépôt d\'alliance';
$lang['fl_deploy_only_your_planets']		= 'Vous ne pouvez déployer des flottes que sur vos planètes !';
$lang['fl_no_enought_deuterium']			= 'Vous n\'avez pas assez de deutérium. Consommation de Deutérium: ';
$lang['fl_no_enought_cargo_capacity']		= 'Vous n\'avez pas assez d\'espace de chargement ! Disponible:';
$lang['fl_admins_cannot_be_attacked']		= 'Les administrateurs et modérateurs de ce jeu ne peuvent pas être attaqués.';
$lang['fl_fleet_sended']					= 'Envoi de flotte';
$lang['fl_from']							= 'Départ';
$lang['fl_arrival_time']					= 'Heure d\'arrivée<br>(cible)';
$lang['fl_return_time']						= 'Heure d\'arriv&eacute;e<br>(retour)';
$lang['fl_fleet']							= 'Flotte';
$lang['fl_player']							= 'Le joueur ';
$lang['fl_add_to_attack']					= ' a été ajouté à l\'attaque.';
$lang['fl_dont_exist']						= ' n\'a pas été ajoutée à l\'attaque.';
$lang['fl_acs_invitation_message']			= ' vous invite à participer à l\'attaque groupée.';
$lang['fl_acs_invitation_title']			= 'Invitation à l\'attaque groupée';
$lang['fl_sac_of_fleet']					= 'Flotte AG';
$lang['fl_modify_sac_name']					= 'Changer le nom de l\'attaque groupée';
$lang['fl_members_invited']					= 'Membres invités';
$lang['fl_invite_members']					= 'Inviter d\'autres membres';
$lang['fl_hours']						= 'heure(s)';

//----------------------------------------------------------------------------//
//BUILDINGS - RESEARCH - SHIPYARD - DEFENSES
$lang['bd_dismantle']						= 'Détruire';
$lang['bd_interrupt']						= 'Interrompre';
$lang['bd_cancel']							= 'Annuler';
$lang['bd_working']							= 'En travail';
$lang['bd_build']							= 'Construire';
$lang['bd_build_next_level']				= 'Construire le Niveau ';
$lang['bd_add_to_list']						= 'Ajouter à la liste';
$lang['bd_no_more_fields']					= 'Plus de place sur la planète';
$lang['bd_remaining']						= 'Ressources restantes';
$lang['bd_lab_required']					= 'Vous devez construire un laboratoire de recherche !';
$lang['bd_building_lab']					= 'Impossible de rechercher quand le laboratoire est en évolution';
$lang['bd_lvl']								= 'niveau';
$lang['bd_spy']								= ' espion';
$lang['bd_commander']						= ' commandant';
$lang['bd_research']						= 'Rechercher';
$lang['bd_shipyard_required']				= 'Vous devez construire un chantier spatial !';
$lang['bd_building_shipyard']				= 'Vous ne pouvez pas construire de vaisseaux pendant l\'évolution du chantier spatial !';
$lang['bd_available']						= 'Disponible: ';
$lang['bd_build_ships']						= 'Construire';
$lang['bd_protection_shield_only_one']		= 'Vous ne pouvez construire qu\'un seul bouclier !';
$lang['bd_build_defenses']					= 'Construire';
$lang['bd_actual_production']				= 'Production actuelle:';
$lang['bd_completed']						= 'Terminé';
$lang['bd_operating']						= '(En travail)';
$lang['bd_continue']						= 'Continuer';
$lang['bd_ready']							= 'Terminé';
$lang['bd_finished']						= 'Terminé';
$lang['bd_work_todo']						= 'File de construction';

//----------------------------------------------------------------------------//
//RESOURCES
$lang['rs_amount']							= 'quantité';
$lang['rs_lvl']								= 'niveau';
$lang['rs_production_on_planet']			= 'Production de matières premières sur la planète "%s"';
$lang['rs_basic_income']					= 'Revenu de base';
$lang['rs_storage_capacity']				= 'Capacité de stockage';
$lang['rs_calculate']						= 'Calculer';
$lang['rs_sum']								= 'Total';
$lang['rs_daily']							= 'Journalière';
$lang['rs_weekly']							= 'Hebdomadaire';

//----------------------------------------------------------------------------//
//OFFICIERS
$lang['of_title']							= 'Officiers';
$lang['of_recruit']							= 'Recruter';
$lang['of_active']							= '<strong><font color="lime">Actif</font></strong>';
$lang['of_inactive']						= '<strong><font color="red">Inactif</font></strong>';

//----------------------------------------------------------------------------//
//TRADER
$lang['tr_only_positive_numbers']			= 'Vous ne pouvez utiliser que des nombres positifs !';
$lang['tr_not_enought_metal']				= 'Vous n\'avez pas assez de métaux.';
$lang['tr_not_enought_crystal']				= 'Vous n\'avez pas assez de cristaux.';
$lang['tr_not_enought_deuterium']			= 'Vous n\'avez pas assez de deutérium.';
$lang['tr_exchange_done']					= 'Échange réussi';
$lang['tr_call_trader']						= 'Appeler un marchand';
$lang['tr_call_trader_who_buys']			= 'Appeler un marchand qui achète du ';
$lang['tr_call_trader_submit']				= 'Appeler marchand';
$lang['tr_exchange_quota']					= 'Le taux d\'échange est de 2/1/0.5';
$lang['tr_sell_metal']						= 'Ventes de métaux';
$lang['tr_sell_crystal']					= 'Ventes de cristaux';
$lang['tr_sell_deuterium']					= 'Ventes de deutérium';
$lang['tr_resource']						= 'Ressource';
$lang['tr_amount']							= 'Quantité';
$lang['tr_quota_exchange']					= 'Taux d\'échange';
$lang['tr_exchange']						= 'Échanger';
$lang['tr_darkmatter_needed']				= 'Vous avez besoin d\'au moins <font color="#FF8900">%s d\'anti-matière</font> pour voir le marchand.';
$lang['tr_full_storage']					= 'Le %s est plein';
$lang['tr_must_put_something']				= 'Vous ne pouvez pas échanger 0 ressources';

//----------------------------------------------------------------------------//
//TECHTREE
$lang['tt_requirements']					= 'Requis';
$lang['tt_lvl']								= 'niveau ';

$lang['tech'] = array(
0 => "Bâtiments",
1 => "Mine de métal",
2 => "Mine de cristal",
3 => "Synthétiseur de deutérium",
4 => "Centrale électrique solaire",
12 => "Centrale électrique de fusion",
14 => "Usine de robots",
15 => "Usine de nanites",
21 => "Chantier spatial",
22 => "Hangar de métal",
23 => "Hangar de cristal",
24 => "Réservoir de deutérium",
31 => "Laboratoire de recherche",
33 => "Terraformeur",
34 => "Dépôt de ravitaillement",
40 => "Bâtiments spéciaux",
41 => "Base lunaire",
42 => "Phalange de capteur",
43 => "Porte de saut spatial",
44 => "Silo de missiles",

100 => "Recherches",
106 => "Technologie Espionnage",
108 => "Technologie Ordinateur",
109 => "Technologie Armes",
110 => "Technologie Bouclier",
111 => "Technologie Protection des vaisseaux spatiaux",
113 => "Technologie Énergie",
114 => "Technologie Hyperespace",
115 => "Réacteur à combustion",
117 => "Réacteur à impulsion",
118 => "Propulsion hyperespace",
120 => "Technologie Laser",
121 => "Technologie Ions",
122 => "Technologie Plasma",
123 => "Réseau de recherche intergalactique",
124 => "Technologie Expéditions",
199 => "Technologie Graviton",

200 => "Vaisseaux",
202 => "Petit transporteur",
203 => "Grand transporteur",
204 => "Chasseur léger",
205 => "Chasseur lourd",
206 => "Croiseur",
207 => "Vaisseau de bataille",
208 => "Vaisseau de colonisation",
209 => "Recycleur",
210 => "Sonde espionnage",
211 => "Bombardier",
212 => "Satellite solaire",
213 => "Destructeur",
214 => "Étoile de la mort",
215 => "Traqueur",

400 => "Défenses",
401 => "Lanceur de missiles",
402 => "Artillerie laser légère",
403 => "Artillerie laser lourde",
404 => "Canon de Gauss",
405 => "Artillerie à ions",
406 => "Lanceur de plasma",
407 => "Petit bouclier",
408 => "Grand bouclier",
502 => "Missile d'interception",
503 => "Missile Interplanétaire",

600 => "Officiers",
601 => "Géologue",
602 => "Amiral",
603 => "Ingénieur",
604 => "Technocrate",
);

$lang['res']['descriptions'] = array(
1 => "Principale matière première pour la construction de bâtiments et de vaisseaux.",
2 => "Principale ressource pour les installations électroniques et pour les alliages.",
3 => "Extrait la petite quantité de deutérium de l'eau d'une planète.",
4 => "Les centrales électriques solaires transforment les rayons de soleil en énergie. Presque tous les bâtiments ont besoin d'énergie pour fonctionner.",
12 => "La centrale électrique de fusion produit de l'énergie en fusionnant 2 atomes d'hydrogène en un atome d'hélium.",
14 => "Les usines de robots produisent des robots ouvriers qui servent à la construction de l'infrastructure planètaire. Chaque niveau augmente la vitesse de construction des différents bâtiments.",
15 => "C'est le perfectionnement de la technologie de robots. Chaque niveau augmente la vitesse de construction des vaisseaux et des bâtiments.",
21 => "Le chantier spatial permet de construire les vaisseaux et les installations de défense.",
22 => "Hangar pour métal avant le traitement.",
23 => "Hangar pour cristal avant le traitement.",
24 => "Réservoirs géants pour le stockage de deutérium.",
31 => "Le laboratoire de recherche est nécessaire pour développer de nouvelles technologies.",
33 => "Le terraformeur permet d'agrandir la surface utile des planètes.",
34 => "Le dépôt de ravitaillement permet le stationnement prolongé de flottes d'autres membres de l'alliance ou de flottes de membres de votre liste d'amis pour augmenter la défense d'une planète. Les flottes restent en orbite et reçoivent le carburant nécessaire depuis ce dépôt.",
41 => "Une lune n'ayant pas d'atmosphère, une base lunaire est nécessaire pour pouvoir commencer la colonisation.",
42 => "La phalange de capteur permet d'observer les mouvements de flotte. Une phalange de niveau élevé a une plus grande portée.",
43 => "Les portes de saut spatial sont d'immenses émetteurs permettant de transporter des vaisseaux à travers la galaxie sans perte de temps.",
44 => "Les silos de missiles servent à stocker les missiles.",

106 => "Cette technologie permet d'obtenir des informations sur les autres planètes de l'univers.",
108 => "Avec l'augmentation des capacités des ordinateurs, plus de flottes peuvent être commandées. Chaque niveau de technologie ordinateur augmente d'une le nombre total de flottes commandables.",
109 => "La technologie armes rend les syst&egrave;mes d'armes plus efficaces. Chaque niveau de technologie armes augmente la puissance des armes des unités par tranche de 10% de la valeur de base.",
110 => "Chaque niveau de la technologie de bouclier augmente l'efficacité des boucliers par tranche de 10%.",
111 => "Des alliages spéciaux rendent les vaisseaux spatiaux de plus en plus résistants. L'efficacité des protections peut être augmentée de 10% par niveau.",
113 => "Maîtriser les différents types d'énergie est nécessaire pour de nombreuses technologies.",
114 => "L'intégration de la 4ème et de la 5ème dimension permet le développement d'un nouveau genre de propulsion plus puissant et efficace.",
115 => "Le développement de ces réacteurs rend les vaisseaux plus rapides mais chaque niveau n'augmente la vitesse que de 10%.",
117 => "Le réacteur &agrave; impulsion est basé sur le principe de réaction disant que la plus grande part de la masse du rayon est gagnée comme sous-produit de la fusion d'atomes qui sert à produire l'énergie nécessaire.",
118 => "Par une déformation spatiale et temporelle dans l'environnement du vaisseau, l'espace est comprimé ce qui permet de parcourir de longues distances dans un minimum de temps.",
120 => "La concentration de lumière créée un rayon pouvant créer des dégâts importants  en touchant un objet.",
121 => "Rayon mortel composé d'ions accélérés. En touchant un objet, il cause des dégâts importants.",
122 => "Une amélioration de la technologie d'ions qui n'accélère pas des ions mais du plasma très énergétique. Ceci a un effet dévastateur en touchant un objet.",
123 => "Les chercheurs de plusieurs planètes utilisent ce réseau pour communiquer.",
124 => "Les vaisseaux peuvent dorénavant être équipés de modules de recherche qui permettent l'évaluation scientifique des données recueillies lors de longues expéditions.",
199 => "Une quantité concentrée de particules de graviton, réseau artificiel de gravitation, est propulsée, capable de détruire des vaisseaux ou même des lunes.",

202 => "Le petit transporteur est un vaisseau très maniable et capable de transporter des matières premières sur d'autres planètes rapidement.",
203 => "Le développement du transporteur augmente la capacité de fret et rend le vaisseau plus rapide que le petit transporteur.",
204 => "Le chasseur léger est un vaisseau très manoeuvrable qui est stationné sur presque toutes les planètes. Les coûts ne sont pas très importants, mais la puissance du bouclier et la capacité de fret sont très limitées.",
205 => "Cette version améliorée du chasseur léger possède une meilleure protection et une capacité d'attaque plus importante.",
206 => "Les croiseurs ont une protection presque trois fois plus importante que celle des chasseurs lourds et leur puissance de tir est plus de deux fois plus grande. De plus, ils sont très rapides.",
207 => "Les vaisseaux de bataille jouent un r&ocirc;le central dans les flottes. Avec leur artillerie lourde, leur vitesse considérable et la grande capacité de fret, ils sont des adversaires respectables.",
208 => "Les nouvelles plan&egrave;tes peuvent &ecirc;tre colonisées avec ce vaisseau.",
209 => "Le recycleur collecte les ressources dans les champs de débris.",
210 => "Les sondes d'espionnage sont des petits drones manoeuvrables qui espionnent les planètes même à grande distance.",
211 => "Le bombardier a été développé pour pouvoir détruire les installations de défense des planètes.",
212 => "Les satellites solaires sont des plates-formes couvertes de cellules solaires, qui se trouvent dans une orbite très élevée et stationnaire. Ils collectent la lumière du soleil et la transmettent par laser à la station de base.",
213 => "Le destructeur est le roi des vaisseaux de guerre.",
214 => "La puissance de destruction de l'étoile de la mort est imbattable.",
215 => "Le traqueur est spécialisé dans l'interception de flottes ennemies.",

401 => "Le lanceur de missiles est une façon simple et bon marché de se défendre.",
402 => "Le bombardement concentré de photons peut causer des dégâts nettement plus important que les armes balistiques habituelles.",
403 => "L'artillerie lourde au laser est l'évolution conséquente de l'artillerie légère au laser.",
404 => "Le canon de Gauss (canon électromagnétique) accélère un projectile pesant des tonnes en consommant une gigantesque quantité d'énergie.",
405 => "L'artillerie d'ions lance des vagues d'ions sur l'objet, ce qui déstabilise les boucliers et endommage l'électronique.",
406 => "Les lanceurs de plasma disposent de la puissance d'une éruption solaire et peuvent donc être plus destructeurs que les destructeurs eux-mêmes.",
407 => "Le petit bouclier couvre toute une planète avec un champ infranchissable qui peut absorber une quantité énorme d'énergie.",
408 => "L'amélioration du petit bouclier peut se servir de nettement plus d'énergie pour se défendre.",
502 => "Le missile interception détruit les missiles adverses.",
503 => "Les missiles interplanétaires détruisent la défense adverse.",

601 => 'Le géologue est un expert reconnu en astrominéralogie et en astrocristallographie. Avec son équipe d\'experts en métallurgie et d\'ingénieurs chimiste, il assiste les gouvernements interplanétaires dans la recherche de nouvelles sources de matières premières et optimise le raffinage de celles-ci.<br><br><strong><font color="lime">+5% de production</font></strong>',
602 => 'L\'amiral de la flotte est un vétéran de guerre et un stratège redouté. Même lorsque le combat est acharné, il garde le sang froid nécessaire pour dominer la situation et est en contact permanent avec les amiraux sous ses ordres. Un empereur responsable ne saurait se passer de l\'amiral de la flotte pour coordonner ses attaques et peut lui faire une telle confiance qu\'il peut envoyer plus de flottes en combat.<br><br><strong><font color="lime">+5% de bouclier, protection des vaisseaux et armes sur les vaisseaux.</font></strong>',
603 => 'L\'ingénieur est un spécialiste de la gestion d\'énergie. En temps de paix, il optimise l\'efficacité des réseaux d\'énergie des colonies.<br><br><strong><font color="lime">+5% d\'énergie.</font></strong>',
604 => 'Les guildes de technocrates sont des scientifiques au génie reconnu. On les trouve aux endroits où la technologie est poussée au delà de ses limites. Personne ne parviendra à décrypter le chiffrement d\'un technocrate, sa seule présence inspire les chercheurs de tout l\'empire.<br><br><strong><font color="lime">-5% de temps de construction des vaisseaux.</font></strong>',
);

//----------------------------------------------------------------------------//
//INFOS
$lang['in_jump_gate_done']					= 'Les opérations de saut se sont bien déroulées, prochain saut possible dans: ';
$lang['in_jump_gate_error_data']			= 'Erreur, il n\'y a aucune donnée de saut !';
$lang['in_jump_gate_not_ready_target']		= 'Les chargeurs d\'énergie de la porte de destination n\'ont pas encore eu le temps de se recharger ! Temps d\'attente: ';
$lang['in_jump_gate_doesnt_have_one']		= 'Vous n\'avez pas de porte de saut sur la lune !';
$lang['in_jump_gate_already_used']			= 'Les chargeurs d\'énergie de la porte de départ n\'ont pas encore eu le temps de se recharger ! Temps d\'attente: ';
$lang['in_jump_gate_available']				= 'disponible';
$lang['in_rf_again']    					= 'Feu rapide contre';
$lang['in_rf_from']     					= 'Feu rapide de';
$lang['in_level']       					= 'Niveau';
$lang['in_prod_p_hour'] 					= 'Production/heure';
$lang['in_difference']  					= 'Différence';
$lang['in_used_energy'] 					= 'Consommation d\'énergie';
$lang['in_prod_energy'] 					= 'Production d\'énergie';
$lang['in_used_deuter']						= 'Consommation de deutérium';
$lang['in_range']       					= 'Portée du détecteur';
$lang['in_title_head']  					= 'Information sur les';
$lang['in_name']        					= 'Nom';
$lang['in_struct_pt']   					= 'Points de structure';
$lang['in_shield_pt']   					= 'Puissance du bouclier';
$lang['in_attack_pt']   					= 'Valeur d\'attaque';
$lang['in_capacity']    					= 'Capacité de fret';
$lang['in_units']       					= 'unités';
$lang['in_base_speed'] 						= 'Vitesse de base';
$lang['in_consumption'] 					= 'Consommation de carburant (Deutérium)';
$lang['in_jump_gate_start_moon']			= 'Lune de départ';
$lang['in_jump_gate_finish_moon']			= 'Lune de destination';
$lang['in_jump_gate_select_ships']			= 'Utiliser la porte de saut spatial: sélection des vaisseaux';
$lang['in_jump_gate_jump']					= 'Sauter';
$lang['in_destroy']     					= 'Détruit:';
$lang['in_needed']      					= 'Requis';
$lang['in_dest_durati'] 					= 'Temps de destruction';

// -------------------------- MINES ------------------------------------------------------------------------------------------------------//
$lang['info'][1]['name']          			= "Mine de métal";
$lang['info'][1]['description']   			= "Principale matière première pour la construction de bâtiments et de vaisseaux. Le métal est la matière première la moins chère, mais c'est la plus importante. La production du métal consomme moins d'énergie que la production des autres ressources. Plus les mines sont développées, plus elles sont profondes. Les réserves minérales sont situées en profondeur sur la majorité des planètes. Les mines les plus profondes permettent d'extraire plus de métal, ce qui augment leur rendement. Cependant, les mines les plus développées consomment aussi plus d'énergie.";
$lang['info'][2]['name']          			= "Mine de cristal";
$lang['info'][2]['description']   			= "Le cristal est la principale ressource pour l'électronique et pour les alliages et son extraction consomme environ une fois et demi plus d'énergie que celle du métal, le cristal est donc plus précieux. Tous les vaisseaux et bâtiments ont besoin de cristal. Malheureusement, la plupart des cristaux nécessaires pour la construction de vaisseaux sont très rares et se trouvent en grande profondeur, comme le métal. La production augmente avec le développement des mines car on atteint des gisement plus grands et plus purs.";
$lang['info'][3]['name']          			= "Synthétiseur de deutérium";
$lang['info'][3]['description']   			= "Le deutérium est produit à partir d'eau lourde. Les plus grandes quantités se trouvent donc en grande profondeur, comme le métal et le cristal. Le développement du bâtiment permet d'accéder à des réserves de deutérium en plus grande profondeur et de concentrer ce deutérium extrait de l'eau lourde. Le deutérium sert de carburant pour les vaisseaux et est nécessaire pour presque toutes les recherches.";

// -------------------------- ENERGY ----------------------------------------------------------------------------------------------------//
$lang['info'][4]['name']          			= "Centrale électrique solaire";
$lang['info'][4]['description']   			= "Pour assurer l'alimentation des mines et des synthétiseurs en énergie, des centrales électriques solaires géantes sont nécessaires. Plus ces installations sont développées, plus la surface de la planète est recouverte de cellules photovoltaïques qui transforment les rayons de soleil en énergie électrique. Les centrales électriques solaires sont la base de l'alimentation énergétique d'une planète.";
$lang['info'][12]['name']         			= "Centrale électrique de fusion";
$lang['info'][12]['description']  			= "Une centrale électrique de fusion (CEF) fonctionne suivant le principe de la fusion sous forte pression et à très haute température de 2 atomes d'hydrogène en un atome d'hélium. Ce procédé produit une quantité incroyable d'énergie en forme de rayonnement, avec 1 gramme d'hydrogène on peut produire 172 MWh d'énergie.<br><br>Plus le réacteur est grand, plus les procédés chimiques liés à cette fusion peuvent être optimisés, et plus il produit d'énergie. Le rendement en énergie peut aussi être optimisé par une augmentation du niveau de la technologie énergie.<br><br>La production en énergie d'une centrale peut être calculée en utilisant la formule suivante :<br>30*[niveau CEF] * (1,05 + [niveau technologie énergie] *0,01) ^ [Niveau CEF]";

// -------------------------- BUILDINGS ----------------------------------------------------------------------------------------------------//
$lang['info'][14]['name']         			= "Usine de robots";
$lang['info'][14]['description']  			= "Les usines de robots produisent des robots ouvriers qui servent à la construction de l'infrastructure planétaire. Chaque niveau augmente la vitesse de construction des différents bâtiments.";
$lang['info'][15]['name']         			= "Usine de nanites";
$lang['info'][15]['description']  			= "L'usine de nanites est la quintessence de la technologie de robots. Les nanites sont des robots dont la dimension ne dépasse pas le nanomètre, mais capables de performances extraordinaires lorsqu'elles travaillent en collaboration. Leur développement augmente la productivité de presque tous les secteurs. Ils peuvent, par exemple, construire des objets à l'échelle moléculaire. Si elles servent dans la construction de vaisseaux, elles deviennent partie intégrante des vaisseaux et peuvent, si elles ont assez de ressources et d'énergie, servir au contrôle des dommages et à la réparation. L'usine de nanites réduit de moitié les temps de construction des bâtiments, des vaisseaux et des unités de défense, à chaque niveau.";
$lang['info'][21]['name']         			= "Chantier spatial";
$lang['info'][21]['description']  			= "Le chantier spatial permet de construire les vaisseaux et les installations de défense. Plus le chantier est grand, plus la construction de vaisseaux et des installations de défense est rapide. La construction d'une usine de nanites permet la production de robots minuscules qui aident les robots ouvriers à travailler plus rapidement.";
$lang['info'][22]['name']         			= "Hangar de métal";
$lang['info'][22]['description']  			= "Hangar géant pour le minerai extrait. Un hangar plus grand permet le stockage d'une plus grande quantité de minerai. Quand le hangar est rempli, l'extraction de minerai s'arrête.";
$lang['info'][23]['name']         			= "Hangar de cristal";
$lang['info'][23]['description']  			= "Le cristal qui n'est pas encore traité est mis en stock dans ces hangars géants. Etant très précieux, ces hangars sont sous haute surveillance. Un hangar plus grand permet le stockage d'une plus grande quantité de cristaux. Lorsque le hangar est plein, l'extraction de cristal est stoppée.";
$lang['info'][24]['name']         			= "Réservoir de deutérium";
$lang['info'][24]['description']  			= "Réservoirs géants pour le stockage de deutérium. Ces réservoirs sont normalement situés près d'un port spatial. Un réservoir plus grand permet le stockage d'une plus grande quantité de deutérium. Lorsque les réservoirs sont pleins, la production de deutérium est stoppée.";
$lang['info'][31]['name']         			= "Laboratoire de recherche";
$lang['info'][31]['description']  			= "Le laboratoire de recherche est nécessaire pour développer de nouvelles technologies. Le niveau du laboratoire détermine la vitesse de la recherche. Pour accélérer la recherche, tous les chercheurs de votre empire sont rassemblés dans le laboratoire de la planète sur laquelle se fait la recherche. Ces chercheurs ne peuvent donc plus faire de recherches sur une autre planète. Dès qu'une nouvelle technologie est développée, les chercheurs retournent à leurs planètes d'origine et emmènent le savoir. Par conséquent, toutes les technologies recherchées peuvent être utilisées sur toutes les planètes.";
$lang['info'][33]['name']         			= "Terraformeur";
$lang['info'][33]['description']  			= "Le développement continu des planètes a soulevé rapidement la question de la limitation de l'espace vital. Les méthodes de construction souterraine et en surface se sont avérées insuffisantes. Un petit groupe composé de physiciens en énergie et d'ingénieurs en technologie de nanites a finalement trouvé la solution: la terraformation.<br>Le terraformeur peut rendre habitable des contrées entières ou même des continents en utilisant de gigantesques quantités d'énergie. Des nanites spécialement développées, assurant une qualité constante du sol, sont produites continuellement dans ce bâtiment.<br><br>Une fois construit, le terraformeur ne peut être détruit.";
$lang['info'][34]['name']         			= "Dépôt de ravitaillement";
$lang['info'][34]['description']  			= "Le dépôt de ravitaillement permet l'approvisionnant en carburant des flottes d'alliés ou d'amis qui participent à votre défense et qui restent en orbite. Chaque niveau du dépôt permet de livrer un montant spécifique de deutérium aux vaisseaux en orbite.";
$lang['info'][41]['name']         			= "Base lunaire";
$lang['info'][41]['description']  			= "Une lune n'ayant pas d'atmosphère, une base lunaire est nécessaire pour pouvoir commencer la colonisation. Celle-ci crée une atmosphère et une gravité artificielle et maintient l'atmosphère à une température supportable. Une base plus grande augmente la surface couverte par la biosphère. Pour chaque niveau de base lunaire, trois champs peuvent être exploités jusqu'à la taille maximale de la lune.<br><br>Une fois construite, la base lunaire ne peut plus être détruite.";
$lang['info'][42]['name']         			= "Phalange de capteur";
$lang['info'][42]['description']  			= "Des capteurs haute définition scannent le spectre complet des fréquences de tous les rayonnements qui atteignent la phalange. Des ordinateurs de haute performance combinent les données de ces minuscules oscillations énergétiques et de cette façon compilent des informations sur les mouvement de vaisseaux sur des planètes éloignées. Un tel scanner a besoin d'énergie sous forme de deutérium (5.000 par scan). Pour faire un tel scan il faut se rendre dans le menu galaxie et cliquer sur sa lune puis cliquer sur une planète ennemie à portée = (niv. phalange)^2 - 1";
$lang['info'][43]['name']         			= "Porte de saut spatial";
$lang['info'][43]['description']  			= "Les portes de saut spatial sont d'immenses émetteurs permettant de transporter même les plus grosses flottes à travers la galaxie sans perte de temps. Cette transmission nécessite une technologie élevée mais pas de deutérium. Par contre, les portes doivent refroidir une heure entre chaque saut, car ceux-ci sont extrêmement exothermes. De plus il est impossible d'envoyer des ressources.";
$lang['info'][44]['name']         			= "Silo de missiles";
$lang['info'][44]['description']  			= "Les silos de missiles servent à stocker les missiles. Chaque niveau de développement permet le stockage de cinq missiles interplanétaires ou de dix missiles d'interception. Un missile interplanétaire occupe la place de deux missiles d'interception. Les types de missiles se combinent à souhait.";

// -------------------------- TECHNOLOGY ----------------------------------------------------------------------------------------------------//
$lang['info'][106]['name']        			= "Technologie Espionnage";
$lang['info'][106]['description'] 			= "La technique d'espionnage est principalement un développement poussé de la technologie des capteurs. Plus cette technique est développée, plus le joueur peut posséder d'informations sur ce qui ce passe dans son environnement. La différence entre le propre niveau d'espionnage et celui de l'ennemi est l'aspect déterminant du succès d'une sonde. Chaque niveau supplémentaire de technique d'espionnage augmente les détails que vous verrez sur vos rapports et réduit les chances que l'ennemi remarquera votre espionnage. En envoyant une grande quantité de sondes par mission, on augmente non seulement les chances d'un rapport détaillé mais aussi les chances de découverte. La technique d'espionnage améliore aussi la localisation des flottes ennemies. En ce qui concerne la localisation, seul votre niveau est déterminant. Dès le niveau 2, l'affichage d'une attaque comportera le nombre de vaisseaux attaquants. Au niveau 4 vous pourrez identifier les types des vaisseaux attaquants et le nombre total des vaisseaux, au niveau 8 le nombre de vaisseaux de chaque type. Cette technologie est indispensable lorsque vous planifiez une attaque, car elle permet de voir si la cible a des flottes et/ou des défenses en place. Cette technique devrait donc être développé très rapidement.";
$lang['info'][108]['name']        			= "Technologie Ordinateur";
$lang['info'][108]['description'] 			= "La technologie ordinateur permet de développer votre infrastructure informatique, les systèmes devenant plus efficaces et plus performants. La vitesse et la performance de calcul augmentent, permettant de commander plus de flottes à la fois. Chaque niveau de technologie ordinateur augmente d'un le nombre total de flottes commandables. Un plus grand nombre de flottes vous permet de raider plus et donc de gagner plus de ressources. Naturellement cette technologie sert aussi aux marchands, ceux-ci pouvant gérer plus de flottes marchandes. C'est pourquoi il est recommandé de continuer de développer cette technologie pendant tout le cours du jeu.";
$lang['info'][109]['name']        			= "Technologie Armes";
$lang['info'][109]['description'] 			= "La technologie armes se concentre surtout sur la mise au point des systèmes d'armes déjà existants. Le but principal est d'approvisionner les systèmes avec plus d'énergie et de concentrer celle-ci. Ceci rend les systèmes d'armes plus efficaces et plus destructeurs. Chaque niveau de technologie armes augmente la puissance des armes des unités par tranche de 10% de la valeur de base. La technologie armes est importante pour tenir ses unités compétitives à long terme. Un développement permanent est recommandé.";
$lang['info'][110]['name']        			= "Technologie Bouclier";
$lang['info'][110]['description'] 			= "La technologie de bouclier se concentre surtout sur le développement de nouvelles possibilités d'approvisionnement des boucliers en énergie et permet donc de les rendre plus efficaces et résistants. Chaque niveau augmente l'efficacité des boucliers par tranche de 10%.";
$lang['info'][111]['name']        			= "Technologie Protection des vaisseaux spatiaux";
$lang['info'][111]['description'] 			= "Des alliages spéciaux rendent les vaisseaux spatiaux de plus en plus résistants. Une fois un alliage puissant développé, la structure moléculaire des vaisseaux est transformée par rayonnement et mise au niveau du meilleur alliage. L'efficacité de la protection augmente de 10% par niveau atteint.";
$lang['info'][113]['name']        			= "Technologie Énergie";
$lang['info'][113]['description'] 			= "La technologie énergie se concentre surtout sur le développement des réseaux et du stockage d'énergie. Une telle technologie bien développée permet de stocker plus d'énergie et de la transporter plus efficacement.";
$lang['info'][114]['name']        			= "Technologie Hyperespace";
$lang['info'][114]['description'] 			= "L'intégration de la 4ème et 5ème dimension permet le développement d'un nouveau genre de propulsion plus puissant et efficace.";
$lang['info'][115]['name']        			= "Réacteur à combustion";
$lang['info'][115]['description'] 			= "Les réacteurs à combustion sont basés sur l'antique technologie des moteurs à réaction. De la matière à température très élevée est expulsée et propulse le vaisseau dans la direction opposée. La portée de ces réacteurs est assez limitée, mais ils sont bon marché, fiables et n'ont guère besoin de maintenance. En outre ils ont besoin de moins de place et se trouvent donc souvent sur des vaisseaux de petite taille. Le développement de ces réacteurs rend les vaisseaux plus rapides mais à chaque niveau la vitesse n'augmente que de 10%. Les réacteurs à combustion étant la base de l'astronautique, il est important de les développer le plus tôt possible. Après, il est très important d'améliorer cette technologie pour améliorer la rapidité des vaisseaux suivants : transporteurs, chasseurs légers, sondes d'espionnage et recycleurs.";
$lang['info'][117]['name']        			= "Réacteur à impulsion";
$lang['info'][117]['description'] 			= "Le réacteur à impulsion se base sur le principe de réaction. Mais la masse rejetée permettant la propulsion n'est qu'un déchet de la fusion d'atomes qui sert à produire l'énergie utilisée par le vaisseau. De la masse supplémentaire peut toutefois être injectée. Chaque niveau de développement de ce type de réacteur permet d'augmenter de 20% la valeur de base de déplacement des vaisseaux suivants: Bombardier, Croiseur, Chasseur lourd et vaisseau de colonisation. Chaque niveau augmente aussi la portée des missiles inter-planétaires.";
$lang['info'][118]['name']        			= "Propulsion hyperespace";
$lang['info'][118]['description'] 			= "Par une déformation spacio-temporelle de l'environnement autour du vaisseau, l'espace est comprimé permettant de parcourir de longues distances dans un minimum de temps. Une propulsion d'hyperespace très développée permet de déformer l'espace encore plus, augmentant la vitesse des vaisseaux de 30% par niveau. Nécessite : Technologie Hyperespace (Niveau 3) Laboratoire de recherche (Niveau 7)";
$lang['info'][120]['name']        			= "Technologie Laser";
$lang['info'][120]['description'] 			= "Le Laser (lumière amplifiée par émission stimulée) crée un rayon intense et extrêmement énergétique de lumière cohérente. Ces installations servent dans beaucoup de domaines, p. e. aux ordinateurs optiques, armes laser qui peuvent détruire la protection d'un vaisseau sans problèmes etc. La technologie laser est une base importante pour le développement d'autres technologies d'armes. Nécessite : Laboratoire de recherche (Niveau 1) Technologie d'énergie (Niveau 2)";
$lang['info'][121]['name']        			= "Technologie Ions";
$lang['info'][121]['description'] 			= "Rayon mortel composé d'ions accélérés. En touchant un objet, il cause des dégâts importants.";
$lang['info'][122]['name']        			= "Technologie Plasma";
$lang['info'][122]['description'] 			= "Une amélioration de la technologie d'ions, n'accélérant pas des ions mais du plasma à haute énergie. Le plasma a un effet extrêmement dévastateur en touchant un objet et s'y déchargeant.";
$lang['info'][123]['name']        			= "Réseau de recherche intergalactique";
$lang['info'][123]['description'] 			= "Les chercheurs de plusieurs planètes utilisent ce réseau pour communiquer.<br>Un laboratoire est ajouté au réseau pour chaque niveau de cette technologie. Les laboratoires les plus développés seront connectés entre eux.<br>Le laboratoire connecté doit lui même posséder le niveau nécessaire pour effectuer cette recherche pour pouvoir participer au réseau. Dans le réseau, les niveaux de développement des laboratoires participants s'additionnent.";
$lang['info'][124]['name']        			= "Technologie Expéditions";
$lang['info'][124]['description'] 			= "Des études approfondies en astrophysique permettent la construction de laboratoires pouvant être installés sur vos vaisseaux. Ceux-ci vous permettront de mener des expéditions dans des endroits inexplorés de la galaxie. Les progrès en astrophysique vous permettent aussi de fonder plus de colonies. Chaque fois que vous augmenterez cette technologie de 2 niveaux, vous pourrez coloniser une planète de plus.";
$lang['info'][199]['name']        			= "Technologie Graviton";
$lang['info'][199]['description'] 			= "Un graviton est une particule qui n'a ni masse ni charge et mais détermine la force de gravitation. En propulsant une quantité concentrée de gravitons, il est possible de créer un champ artificiel de gravitation, fonctionnant comme un trou noir. Ce champ attire tous les objets environnant possédant une masse et les écrase, pouvant détruire des vaisseaux et même des lunes. Pour produire une quantité suffisante de gravitons, il faut une quantité incommensurable d'énergie.<br>Nécessite : Laboratoire de recherche (Niveau 12)";

// -------------------------- SHIPS ----------------------------------------------------------------------------------------------------//
$lang['info'][202]['name']        			= "Petit transporteur";
$lang['info'][202]['description'] 			= "Les petits transporteurs ont à peu près la même taille que les chasseurs, mais n'ont pas de propulsion puissante ou d'armes, afin d'avoir plus de place pour le fret. Un vaisseau de type petit transporteur peut transporter 5.000 unités de ressources. Le grand transporteur peut transporter cinq fois plus de fret. En même temps sa protection et sa propulsion sont plus puissantes. En raison de leur puissance de tir limitée, les vaisseaux de type petit transporteur sont souvent escortés par d'autres vaisseaux.<br><br>Le développement du réacteur à impulsion au niveau 5 permet de rééquiper le petit transporteur avec ce type de réacteurs, accélérant alors sensiblement ce vaisseau.";
$lang['info'][203]['name']        			= "Grand transporteur";
$lang['info'][203]['description'] 			= "Ce vaisseau n'a guère d'armes ou d'autres technologies à bord. Il est donc préférable de lui fournir une escorte qui lui permet de profiter de toute sa capacité de fret. Avec son réacteur de combustion de haute performance, le grand transporteur sert pour transporter rapidement des ressources entre les planètes et bien sûr il accompagne les flottes durant leurs attaques sur d'autres planètes pour pouvoir piller le maximum de ressources.";
$lang['info'][204]['name']        			= "Chasseur léger";
$lang['info'][204]['description'] 			= "Le chasseur léger est un vaisseau très manoeuvrable qui est stationné sur presque toutes les planètes. Il ne coûte pas trop cher, mais la puissance de son bouclier et sa capacité de fret sont très limitées.";
$lang['info'][205]['name']        			= "Chasseur lourd";
$lang['info'][205]['description'] 			= "La propulsion conventionnelle n'était plus suffisante pour les chasseurs lourds. Pour rendre les futurs vaisseaux plus rapides, les ingénieurs eurent recours au réacteur à impulsions. Celui-ci augmente certes les coûts de production, mais élargit les possibilités. L'arrivée de cette propulsion permet d'utiliser plus d'énergie pour les armes et la protection, de plus ces vaisseaux sont produits avec des matériaux d'une meilleure qualité. Ceci améliore l'intégrité structurelle et la puissance de tir. Le Chasseur Lourd est donc un vaisseau beaucoup plus menaçant que son petit frère, le chasseur léger. Ces changements font du chasseur lourd la base technologique sur laquelle a été développé le croiseur.";
$lang['info'][206]['name']        			= "Croiseur";
$lang['info'][206]['description'] 			= "Le développement des lasers lourds et des canons d'ions sonnèrent le glas de l'époque des chasseurs. Malgré de nombreuses modifications, la puissance des armes et de la protection ne purent être assez améliorées pour pouvoir s'opposer à ces canons de défense. Il fut donc décidé de construire une nouvelle classe de vaisseaux avec plus de protection et une plus grande puissance de feu. Le croiseur était né. Les croiseurs ont une protection quasiment trois fois supérieure à celle des chasseurs lourds et leur puissance de feu est plus que deux fois supérieure. De plus, ils sont très rapides. Il n'y a pas d'arme plus puissante contre une défense moyenne. Les croiseurs ont dominé l'espace pendant presque un siècle. L'apparition de l'artillerie électromagnétique et des lanceurs de plasma a mis fin à leur domination. Pourtant ils servent encore souvent dans les batailles contre les unités de chasseurs.";
$lang['info'][207]['name']        			= "Vaisseau de bataille";
$lang['info'][207]['description'] 			= "Les vaisseaux de bataille jouent un rôle central dans les flottes. Avec leur artillerie lourde, leur vitesse considérable et leur grande capacité de fret, ils sont des adversaires imposant le respect.";
$lang['info'][208]['name']        			= "Vaisseau de colonisation";
$lang['info'][208]['description'] 			= "Avec ce vaisseau, vous pouvez conquérir de nouvelles planètes - une nécessité pour tout empire en expansion. Lorsque le vaisseau atterri sur une planète inconnue, il se transforme en base. Il produit alors assez de ressources pour assurer le développement de la nouvelle colonie. Le nombre maximal de planètes pouvant être colonisées est déterminé par votre niveau en astrophysique. Par tranche de 2 niveaux dans cette technologie, il vous est possible de coloniser une planète supplémentaire.";
$lang['info'][209]['name']        			= "Recycleur";
$lang['info'][209]['description'] 			= "Les dimensions des batailles spatiales se sont constamment élargies. Des milliers de vaisseaux ont été construits, mais les Champs de débris semblaient être perdus pour toujours. Les transporteurs ne pouvaient pas s'approcher des Champs de débris sans prendre le risque d'être endommagés considérablement par des détritus. Un nouveau développement dans le domaine de la technologie des boucliers a permis de construire cette nouvelle classe de vaisseau comparable aux grands transporteurs, les recycleurs. Grâce au recycleur, les ressources qui semblaient être perdues peuvent quand même être récupérées. Même les décombres de petite taille ne les menacent pas grâce à leurs nouveaux boucliers. Malheureusement ces installations ont besoin d'espace ce qui limite la capacité de fret à 20.000 unités.";
$lang['info'][210]['name']       			= "Sonde d'espionnage";
$lang['info'][210]['description'] 			= "Les sondes d'espionnage sont des petits drones manoeuvrables qui espionnent les planètes même à grande distance. Leurs réacteurs de haute performance leur permettent de parcourir de longues distances en quelques secondes. Dès qu'elles atteignent l'orbite d'une planète elles s'y installent et l'espionnent. Pendant cette activité, l'ennemi peut facilement les découvrir et attaquer. Pour limiter leur taille, elles n'ont pas de protection, de bouclier ou d'armes, c'est pourquoi elles sont faciles à détruire.";
$lang['info'][211]['name']        			= "Bombardier";
$lang['info'][211]['description'] 			= "Le bombardier a été développé pour pouvoir détruire les installations de défense des planètes. Grâce à une lunette laser, il peut larguer des bombes de plasma avec une précision chirurgicale sur la surface des planètes et y causer d'énormes dégâts.<br><br>Le développement de la propulsion hyperespace au niveau 8 permet d'équiper le bombardier avec ce type de propulseurs, améliorant alors sensiblement sa vitesse.";
$lang['info'][212]['name']        			= "Satellite solaire";
$lang['info'][212]['description'] 			= "Les satellites solaires sont positionnés dans une orbite géostationnaire autour d'une planète. Ils collectent la lumière du soleil et la transmettent grâce à un système de miroirs à la station de base. L'efficacité des satellites solaires dépend de la lumière du soleil. Naturellement, la quantité d'énergie est plus grande quand l'orbite est proche du soleil. Avec leur bon rapport qualité/prix, les satellites solaires sont la solution pour les problèmes d'énergie de beaucoup de planètes. Attention: Les satellites solaires peuvent être détruits pendant une bataille.";
$lang['info'][213]['name']        			= "Destructeur";
$lang['info'][213]['description'] 			= "Le destructeur est le roi des vaisseaux de guerre. Ses tours de guerre avec artillerie ion, plasma et électromagnétique peuvent, grâce à ses systèmes d'acquisition de cible, toucher des chasseurs rapides avec presque 99% de certitude. Comme ils sont très grands, leur maneouvrabilité est très limitée. Pendant une bataille ils sont donc plus comparables à une station de guerre qu'à un vaisseau de guerre. Leur consommation de deutérium est aussi grande que leur importance dans une bataille.";
$lang['info'][214]['name']        			= "Étoile de la mort";
$lang['info'][214]['description'] 			= "L'étoile de la mort est équipée d'une artillerie géante de gravitons qui permet de détruire des vaisseaux de la taille des destructeurs ou même d'une lune. Comme cela nécessite une quantité d'énergie gigantesque, elle se compose presque entièrement de générateurs. Un vaisseau de cette taille et de cette puissance a besoin d'une gigantesque quantité de ressources et d'ouvriers qui ne peuvent être fournis que par des empires spatiaux puissants.";
$lang['info'][215]['name']        			= "Traqueur";
$lang['info'][215]['description'] 			= "Ce vaisseau a été conçu pour combattre les flottes ennemies. Ses armes laser nouvelle génération le rendent capable d'affronter un grand nombre de vaisseaux en même temps. A cause de son fuselage étroit et de son armement important, sa capacité de transport de ressources est très limitée. Mais sa consommation en carburant est très faible.";

// -------------------------- DEFENSES ----------------------------------------------------------------------------------------------------//
$lang['info'][401]['name']        			= "Lanceur de missiles";
$lang['info'][401]['description'] 			= "Le lanceur de missiles est une façon simple et bon marché de se défendre. Comme il n'est qu'une évolution d'armes balistiques conventionnelles, il n'a pas besoin de recherche. Ses faibles frais de production permettent de s'en servir pour la défense contre des petites flottes, par contre au fur et à mesure il perd de son importance. Après il ne sert qu'à intercepter des missiles. Des rumeurs existent affirmant que les militaires sont en train de développer de nouveaux lanceurs. Les installations de défense sont désactivées dès qu'elles sont trop endommagées. Après une bataille, jusqu'à 70% des systèmes endommagés peuvent être réparés.";
$lang['info'][402]['name']        			= "Artillerie laser légère";
$lang['info'][402]['description'] 			= "Pour pouvoir rivaliser avec les développements énormes de la technologie Vaisseaux, les chercheurs ont dû développer un système de défense capable de battre des vaisseaux plus grands et mieux équipés. C'est ainsi qu'est né le laser léger. Le bombardement concentré de photons peut causer des dégâts nettement plus importants que les armes balistiques habituelles. De plus, on l'a aussi équipé d'un bouclier plus puissant pour pouvoir résister aux nouvelles classes de vaisseaux. Pour garder les frais de production raisonnables, la structure n'a pas été renforcée. Le laser léger offre une performance importante par rapport à son faible coût et est donc est très intéressant, même pour des civilisations plus développées. Les installations de défense sont désactivées dès qu'elles sont trop endommagées. Après une bataille, jusqu'à 70% des systèmes endommagés peuvent être réparés.";
$lang['info'][403]['name']        			= "Artillerie laser lourde";
$lang['info'][403]['description'] 			= "L'artillerie lourde au laser est l'évolution logique de l'artillerie légère au laser. La structure a été renforcée et améliorée avec de nouveaux matériaux. La structure est donc plus résistante. En même temps, le système d'énergie et l'ordinateur de cible ont été aussi améliorés, ce qui permet de concentrer plus d'énergie sur un objet. Les installations de défense sont désactivées dès qu'elles sont trop endommagées. Après une bataille jusqu'à 70% des systèmes endommagés peuvent être réparés.";
$lang['info'][404]['name']        			= "Canon de Gauss";
$lang['info'][404]['description'] 			= "On a longtemps pensé que la technologie des armes à projectile était complètement obsolète, à l'heure de la fusion nucléaire, des alliages ultra-résistants et des boucliers de force. Pourtant le principe, déjà connu au 20ième et au 21ième siècle, d'accélération de particules a été révolutionné et remis au goût du jour par des découvertes dans le domaine de la technologie énergie, permettant le développement du canon de Gauss (Canon électromagnétique). Celui-ci n'est en fait rien d'autre qu'une version nettement plus grande et puissante du canon. Des projectiles pesant des tonnes sont accélérés magnétiquement et atteignent une vitesse telle que les particules autour du projectile s'enflamment et le recul fait trembler la terre. Même les protections et boucliers modernes ont du mal à résister à cette force brute, et il n'est pas rare qu'un projectile traverse complètement un objet. Les installations de défense se désactivent dès qu'elles sont trop endommagées. Après une bataille, jusqu'à 70% des systèmes endommagés peuvent être réparés.";
$lang['info'][405]['name']        			= "Canon à ions";
$lang['info'][406]['description'] 			= "Déjà au 21eme siècle terrestre, on connaissait la technologie EMP. Les EMP sont les Impulsion électromagnétiques (ElectroMagnetic Pulse) pouvant détruire les appareils électroniques en y créant des tensions électriques de grande amplitude permettant par exemple de bloquer tous les appareils sensibles. À l'époque, les EMP étaient logés dans des missiles ou des bombes, ou le résultat d'une explosion atomique. Depuis, les EMP ont été améliorés pour rendre des vaisseaux incapables d'agir sans les détruire et donc en prendre possession. Aujourd'hui, l'artillerie à ions est la version la plus moderne des EMP. Elle lance une vague d'ions (des particules chargées électriquement) sur sa cible, déstabilisant les boucliers et l'électronique dont les systèmes de survie. Sa puissance cinétique n'est pas importante. Les croiseurs se servent eux aussi de la technologie ions, c'est d'ailleurs le seul type de vaisseau, les autres types ne disposant pas de suffisamment d'énergie. Il est souvent intéressant de ne pas détruire un vaisseau mais de le paralyser. Les systèmes de défense se désactivent dès qu'ils sont trop endommagés. Après une bataille jusqu'à 70% des systèmes endommagés peuvent être réparés.";
$lang['info'][406]['name']        			= "Lanceur de plasma";
$lang['info'][406]['description'] 			= "Les technologies laser et ions ayant été portées à un niveau de perfection ne permettant plus d'améliorations significatives, les scientifiques trouvèrent un moyen de rendre les défenses encore plus efficaces : en combinant ces 2 technologies. Comme dans le cas de la fusion nucléaire, des particules (généralement de Deutérium) sont portées à très hautes températures. La technologie des ions permet de charger électriquement ces particules, de les stabiliser, les confiner et les accélérer. La charge de particules devant être projetées est portée à haute température, ionisée, mise sous pression et catapultée dans l'espace dans une superbe gerbe de flammes bleues. Il est probable que l'équipage du vaisseau ciblé ne soit pas heureux de pouvoir assister à un tel spectacle pyrotechnique. Le lanceur de plasma est une des armes les plus menaçantes, mais cette technologie est assez chère. Les systèmes de défense se désactivent dès qu'ils sont trop endommagés. Après une bataille jusqu'à 70% des systèmes endommagés peuvent être réparés.";
$lang['info'][407]['name']        			= "Petit bouclier";
$lang['info'][407]['description'] 			= "Longtemps avant l'installation des générateurs de bouclier sur des vaisseaux, existaient déjà des générateurs géants sur la surface des planètes. Ceux-ci permettaient de couvrir les planètes avec des champs de force infranchissables qui pouvaient absorber des dégâts énormes avant de s'effondrer. Des petites flottes d'attaques échouent souvent contre ces boucliers. Ces boucliers peuvent être améliorés. Après, on peut même construire un grand bouclier qui est encore plus puissant. Pour chaque planète on ne peut construire qu'un seul bouclier.";
$lang['info'][408]['name']        			= "Grand bouclier";
$lang['info'][408]['description'] 			= "L'amélioration du petit bouclier. Il est basé sur la même technologie mais peut se servir de nettement plus d'énergie pour se défendre.";
$lang['info'][502]['name']        			= "Missile d'interception";
$lang['info'][502]['description'] 			= "Le missile d'interception détruit les missiles adverses. Chaque missile d'interception détruit un missile interplanétaire.";
$lang['info'][503]['name']        			= "Missile interplanétaire";
$lang['info'][503]['description'] 			= "Les missiles interplanétaires détruisent la défense adverse. Les systèmes de défense détruits par des missiles interplanétaires ne pas être réparés.";

// -------------------------- OFFICIERS ----------------------------------------------------------------------------------------------------//
$lang['info'][601]['name']        			= "Géologue";
$lang['info'][601]['description'] 			= "Le géologue est un expert reconnu en astrominéralogie et en astrocristallographie. Avec son équipe d'experts en métallurgie et d'ingénieurs chimiste, il assiste les gouvernements interplanétaires dans la recherche de nouvelles sources de matières premières et optimise le raffinage de celles-ci.";
$lang['info'][602]['name']        			= "Amiral";
$lang['info'][602]['description'] 			= "L'amiral de la flotte est un vétéran de guerre et un stratège redouté. Même lorsque le combat est acharné, il garde le sang froid nécessaire pour dominer la situation et est en contact permanent avec les amiraux sous ses ordres. Un empereur responsable ne saurait se passer de l'amiral de la flotte pour coordonner ses attaques et peut lui faire une telle confiance qu'il peut envoyer plus de flottes en combat.";
$lang['info'][603]['name']        			= "Ingénieur";
$lang['info'][603]['description'] 			= "L'ingénieur est un spécialiste de la gestion d'énergie. En temps de paix, il optimise l'efficacité des réseaux d'énergie des colonies.";
$lang['info'][604]['name']        			= "Technocrate";
$lang['info'][604]['description'] 			= "Les guildes de technocrates sont des scientifiques au génie reconnu. On les trouve aux endroits où la technologie est poussée au delà de ses limites. Personne ne parviendra à déchiffrer le cryptage d'un technocrate, sa seule présence inspire les chercheurs de tout l'empire.";

//----------------------------------------------------------------------------//
//MESSAGES
$lang['mg_type'][0]    						= 'Rapport(s) d\'espionnage(s)';
$lang['mg_type'][1]    						= 'Message(s) de joueur(s)';
$lang['mg_type'][2]    						= 'Message(s) d\'alliance';
$lang['mg_type'][3]    						= 'Rapport(s) de combat(s)';
$lang['mg_type'][4]    						= 'Rapport(s) d\'exploitation(s)';
$lang['mg_type'][5]    						= 'Transport(s) de flotte(s)';
$lang['mg_type'][15]   						= 'Rapport(s) d\'expédition(s)';
$lang['mg_type'][99]   						= 'Rapport(s) Liste de construction';
$lang['mg_type'][100]  						= 'Afficher tous les messages';
$lang['mg_no_subject']						= 'Pas de sujet';
$lang['mg_no_text']							= 'Pas de message';
$lang['mg_msg_sended']						= 'Message envoyé';
$lang['mg_delete_marked']					= 'Supprimer les messages sélectionnés';
$lang['mg_delete_unmarked']					= 'Supprimer tous les messages non sélectionnés';
$lang['mg_delete_all']						= 'Supprimer tous les messages';
$lang['mg_show_only_header_spy_reports']	= 'Montrer uniquement une partie du rapport d\'espionnage';
$lang['mg_action']							= 'Action';
$lang['mg_date']							= 'Date';
$lang['mg_from']							= 'De';
$lang['mg_subject']							= 'Sujet';
$lang['mg_confirm_delete']					= 'Confirmer';
$lang['mg_message_title']					= 'Messages';
$lang['mg_message_type']					= 'Type de message';
$lang['mg_total']							= 'Total';
$lang['mg_game_operators']					= 'Opérateurs';
$lang['mg_to']								= 'Pour';
$lang['mg_send_message']					= 'Envoyer un message';
$lang['mg_message']							= 'Message';
$lang['mg_chars']							= 'caractères';
$lang['mg_send']							= 'Envoyer';

//----------------------------------------------------------------------------//
//ALLIANCE
$lang['al_description_message']				= 'Message de description de l\'alliance';
$lang['al_web_text']						= 'Site web de l\'alliance';
$lang['al_request']							= 'Candidatures';
$lang['al_click_to_send_request']			= 'Cliquer ici pour déposer votre candidature';
$lang['al_tag_required']					= 'L\'alliance n\'a pas de TAG !';
$lang['al_name_required']					= 'L\'alliance n\'a pas de nom !';
$lang['al_already_exists']					= 'L\'alliance %s existe déjà.';
$lang['al_created']							= 'L\'alliance %s a été créée.';
$lang['al_continue']						= 'Continuer';
$lang['al_alliance_closed']					= 'L\'alliance ne supporte pas de membres supplémentaires.';
$lang['al_request_confirmation_message']	= 'Votre candidature a été sauvegardée. Vous recevrez un message si vous êtes accepté. <br><a href="game.php?page=alliance">Retour</a>';
$lang['al_default_request_text']			= 'La direction de l\'alliance n\'a pas crée de modèle de candidature.';
$lang['al_write_request']					= 'Envoyer une candidature à l\'alliance %s';
$lang['al_request_deleted']					= 'Votre candidature à l\'alliance %s a été supprimée. <br/> Maintenant vous pouvez écrire une nouvelle candidature ou créer votre propre alliance.';
$lang['al_request_wait_message']			= 'Vous avez déjà deposé votre candidature à l\'alliance %s. <br/> Veuillez attendre d\'obtenir une réponse ou retirer votre candidature.';
$lang['al_delete_request']					= 'Supprimer la candidature';
$lang['al_founder_cant_leave_alliance']		= 'Le fondateur ne peut pas abandonner l\'alliance.';
$lang['al_leave_sucess']					= 'Vous avez quitté l\'alliance %s.';
$lang['al_do_you_really_want_to_go_out']	= 'Voulez-vous vraiment quitter l\'alliance %s?';
$lang['al_go_out_yes']						= 'Oui';
$lang['al_circular_sended']					= 'Message collectif envoyé. Les joueurs suivants ont reçu le message:';
$lang['al_all_players']						= 'Tous les joueurs';
$lang['al_no_ranks_defined']				= 'Il n\'y a pas de rang défini';
$lang['al_request_text']					= 'Texte de candidature';
$lang['al_inside_text']						= 'Texte interne';
$lang['al_outside_text']					= 'Texte externe';
$lang['al_transfer_alliance']				= 'Transférer l\'alliance';
$lang['al_disolve_alliance']				= 'Dissoudre l\'alliance';
$lang['al_founder_rank_text']				= 'Fondateur';
$lang['al_new_member_rank_text']			= 'Novice';
$lang['al_acept_request']					= 'Accepter';
$lang['al_you_was_acceted']					= 'Vous avez été accepté dans ';
$lang['al_hi_the_alliance']					= 'Bonjour !<br>L\'alliance <b>';
$lang['al_has_accepted']					= '</b> a accepté votre candidature.<br>Message du fondateur: <br>';
$lang['al_decline_request']					= 'Rejeter';
$lang['al_you_was_declined']				= 'Vous avez été refusé dans ';
$lang['al_has_declined']					= '</b> a refusé votre candidature.<br>Message du fondateur: <br>';
$lang['al_no_requests']						= 'Aucune candidature';
$lang['al_request_from']					= 'Candidature de "%s"';
$lang['al_no_request_pending']				= 'Il y a %n candidature/s en attente.';
$lang['al_name']							= 'nom';
$lang['al_new_name']						= 'Nouveau nom';
$lang['al_tag']								= 'tag';
$lang['al_new_tag']							= 'Nouveau tag';
$lang['al_user_list']						= 'Liste des membres';
$lang['al_manage_alliance']					= 'Administrer l\'alliance';
$lang['al_send_circular_message']			= 'Envoyer un message collectif';
$lang['al_new_requests']					= 'nouvelle/s candidature/s';
$lang['al_save']							= 'Sauvegarder';
$lang['al_dlte']							= 'Supprimer';
$lang['al_rank_name']						= 'Nom du rang';
$lang['al_ok']								= 'OK';
$lang['al_number_of_records']				= 'Nombre';
$lang['al_num']								= 'N°';
$lang['al_member']							= 'Nom';
$lang['al_message']							= 'Message';
$lang['al_position']						= 'Rang';
$lang['al_points']							= 'Points';
$lang['al_coords']							= 'Coords';
$lang['al_member_since']					= 'Adhésion';
$lang['al_estate']							= 'Statut';
$lang['al_back']							= 'Retour';
$lang['al_actions']							= 'Actions';
$lang['al_change_title']					= 'Changer';
$lang['al_the_alliance']					= 'de l\'alliance';
$lang['al_change_submit']					= 'Changer';
$lang['al_reply_to_request']				= 'Réaction à cette candidature';
$lang['al_reason']							= 'Raison';
$lang['al_characters']						= 'caractères';
$lang['al_request_list']					= 'Liste des candidatures';
$lang['al_candidate']						= 'Nom';
$lang['al_request_date']					= 'Date de la candidature';
$lang['al_transfer_alliance']				= 'Abandonner/Transférer l\'alliance?';
$lang['al_transfer_to']						= 'Transférer à';
$lang['al_transfer_submit']					= 'Transférer';
$lang['al_ally_information']				= 'Information alliance';
$lang['al_ally_info_tag']					= 'TAG';
$lang['al_ally_info_name']					= 'Nom';
$lang['al_ally_info_members']				= 'Membres';
$lang['al_your_request_title']				= 'Votre candidature';
$lang['al_applyform_send']					= 'Envoyer';
$lang['al_applyform_reload']				= 'Exemple';
$lang['al_circular_send_ciruclar']			= 'Envoyer un message collectif';
$lang['al_receiver']						= 'Destinataire';
$lang['al_circular_send_submit']			= 'Envoyer';
$lang['al_circular_reset']					= 'Effacer';
$lang['al_alliance']						= 'Alliances';
$lang['al_alliance_make']					= 'Fonder sa propre alliance';
$lang['al_alliance_search']					= 'Rechercher une alliance';
$lang['al_your_ally']						= 'Votre alliance';
$lang['al_rank']							= 'Votre rang';
$lang['al_web_site']						= 'Page d\'accueil';
$lang['al_inside_section']					= 'Texte interne';
$lang['al_make_alliance']					= 'Fonder sa propre alliance';
$lang['al_make_ally_tag_required']			= 'TAG de l\'alliance (3-8 caractères)';
$lang['al_make_ally_name_required']			= 'Nom de l\'alliance (3-30 caractères)';
$lang['al_make_submit']						= 'Créer';
$lang['al_find_alliances']					= 'Chercher une alliance';
$lang['al_find_text']						= 'Chercher :';
$lang['al_find_submit']						= 'Rechercher';
$lang['al_the_nexts_allys_was_founded']		= 'Nous avons trouvé les alliances suivantes:';
$lang['al_manage_ranks']					= 'Configurer les droits';
$lang['al_manage_members']					= 'Administrer les membres';
$lang['al_manage_change_tag']				= 'Changer le TAG de l\'alliance';
$lang['al_manage_change_name']				= 'Changer le nom de l\'alliance';
$lang['al_texts']							= 'Administration du texte';
$lang['al_manage_options']					= 'Options';
$lang['al_manage_image']					= 'Logo de l\'alliance';
$lang['al_manage_requests']					= 'Candidatures';
$lang['al_requests_not_allowed']			= 'impossible (alliance fermée)';
$lang['al_requests_allowed']				= 'possible (alliance ouverte)';
$lang['al_manage_founder_rank']				= 'Rang du fondateur';
$lang['al_configura_ranks']					= 'Configurer les droits';
$lang['al_create_new_rank']					= 'Créer un nouveau rang';
$lang['al_rank_name']						= 'Nom du rang';
$lang['al_create']							= 'Créer';
$lang['al_legend']							= 'Liste des droits';
$lang['al_legend_disolve_alliance']			= 'Dissoudre l\'alliance';
$lang['al_legend_kick_users']				= 'Expulser un joueur';
$lang['al_legend_see_requests']				= 'Voir les candidatures';
$lang['al_legend_see_users_list']			= 'Voir la liste des membres';
$lang['al_legend_check_requests']			= 'Gérer les candidatures';
$lang['al_legend_admin_alliance']			= 'Administrer l\'alliance';
$lang['al_legend_see_connected_users']		= 'Voir statut des joueurs dans la liste des membres';
$lang['al_legend_create_circular']			= 'Envoyer un message collectif';
$lang['al_legend_right_hand']				= '"Bras droit" (nécessaire pour transmettre le statut de fondateur)';

$lang['al_requests']						= 'Candidatures';
$lang['al_circular_message']				= 'Message collectif';
$lang['al_leave_alliance']					= 'Quitter l\'alliance';

//----------------------------------------------------------------------------//
//BUDDY
$lang['bu_request_exists']					= 'Un demande d\'ami existe déjà pour ce joueur !';
$lang['bu_cannot_request_yourself']			= 'Vous ne pouvez pas vous envoyez d\'invitation...';
$lang['bu_request_message']					= 'Texte de la demande';
$lang['bu_player']							= 'Joueur';
$lang['bu_request_text']					= 'Demande d\'ami';
$lang['bu_characters']						= 'caractères';
$lang['bu_back']							= 'Retour';
$lang['bu_send']							= 'Envoyer';
$lang['bu_cancel_request']					= 'Refuser la demande';
$lang['bu_accept']							= 'Accepter';
$lang['bu_decline']							= 'Rejeter';
$lang['bu_connected']						= 'Connecté';
$lang['bu_fifteen_minutes']					= '15 min.';
$lang['bu_disconnected']					= 'Déconnecté';
$lang['bu_delete']							= 'Supprimer';
$lang['bu_buddy_list']						= 'Liste d\'ami';
$lang['bu_requests']						= 'Demandes';
$lang['bu_alliance']						= 'Alliance';
$lang['bu_coords']							= 'Coordonnées';
$lang['bu_text']							= 'Texte';
$lang['bu_action']							= 'Action';
$lang['bu_my_requests']						= 'Mes demandes';
$lang['bu_partners']						= 'Amis';
$lang['bu_estate']							= 'État';
$lang['bu_deleted_title']					= 'Ami perdu';
$lang['bu_deleted_text']					= 'Le joueur %u vous a supprimé de sa liste d\'amis.';
$lang['bu_accepted_title']					= 'Demande acceptée';
$lang['bu_accepted_text']					= 'Le joueur %u a accepté votre demande d\'ami.';
$lang['bu_rejected_title']					= 'Demande refusée';
$lang['bu_rejected_text']					= 'Le joueur %u a refusé votre demande d\'ami.';
$lang['bu_to_accept_title']					= 'Nouvelle demande d\'ami';
$lang['bu_to_accept_text']					= 'Le joueur %u vous a envoyé une demande d\'ami.';

//----------------------------------------------------------------------------//
//NOTES
$lang['nt_important']						= 'haute';
$lang['nt_normal']							= 'normale';
$lang['nt_unimportant']						= 'basse';
$lang['nt_create_note']						= 'Créer une note';
$lang['nt_edit_note']						= 'Éditer une note';
$lang['nt_you_dont_have_notes']				= 'Il n\'y a pas de notes.';
$lang['nt_notes']							= 'Notes';
$lang['nt_create_new_note']					= 'Créer une nouvelle note';
$lang['nt_date_note']						= 'Date';
$lang['nt_subject_note']					= 'Sujet';
$lang['nt_size_note']						= 'Taille';
$lang['nt_dlte_note']						= 'Supprimer';
$lang['nt_priority']						= 'Priorité';
$lang['nt_note']							= 'Notice';
$lang['nt_characters']						= 'caractères';
$lang['nt_back']							= 'Retour';
$lang['nt_reset']							= 'Rétablir';
$lang['nt_save']							= 'Sauvegarder';

//----------------------------------------------------------------------------//
//STATISTICS
$lang['st_player']							= 'Joueur';
$lang['st_alliance']						= 'Alliance';
$lang['st_points']							= 'Points';
$lang['st_fleets']							= 'Flottes';
$lang['st_researh']							= 'Recherches';
$lang['st_buildings']						= 'Bâtiments';
$lang['st_defenses']						= 'Défenses';
$lang['st_position']						= 'Place';
$lang['st_members']							= 'Membre';
$lang['st_per_member']						= 'Par membre';
$lang['st_statistics']						= 'Statistiques ';
$lang['st_updated']							= 'MAJ ';
$lang['st_show']							= 'voir';
$lang['st_per']								= 'par';
$lang['st_in_the_positions']				= 'classé dans';

//----------------------------------------------------------------------------//
//SEARCH
$lang['sh_tag']								= 'Tag';
$lang['sh_name']							= 'Nom';
$lang['sh_members']							= 'Membre';
$lang['sh_points']							= 'Points';
$lang['sh_searcg_in_the_universe']			= 'Recherche univers';
$lang['sh_player_name']						= 'Nom du joueur';
$lang['sh_planet_name']						= 'Nom de la planète';
$lang['sh_alliance_tag']					= 'TAG de l\'alliance';
$lang['sh_alliance_name']					= 'Nom de l\'alliance';
$lang['sh_search']							= 'Rechercher';
$lang['sh_buddy_request']					= 'Demande d\'ami';
$lang['sh_alliance']						= 'Alliance';
$lang['sh_planet']							= 'Planète';
$lang['sh_coords']							= 'Position';
$lang['sh_position']						= 'Rang';

//----------------------------------------------------------------------------//
//OPTIONS
$lang['op_cant_activate_vacation_mode']		= 'Si vous construisez ou vous déplacez des flottes vous ne serez pas en mesure d\'entrer en mode vacances.';
$lang['op_password_changed']				= 'Le mot de passe a bien été changé.<br /><a href="index.php" target="_top">Retour</a>';
$lang['op_username_changed']				= 'Le nom du compte a bien été changé.<br /><a href="index.php" target="_top">Retour</a>';
$lang['op_options_changed']					= 'Les changements ont été sauvegardé.<br /><a href="game.php?page=options">Retour</a>';
$lang['op_vacation_mode_active_message']	= 'Le mode vacances est activé. Actif jusqu\'au : ';
$lang['op_end_vacation_mode']				= 'Retour de vacances';
$lang['op_save_changes']					= 'Sauvegarder les changements';
$lang['op_admin_title_options']				= 'Options réservées à l\'administration';
$lang['op_admin_planets_protection']		= 'Protection des planètes';
$lang['op_user_data']						= 'Informations sur le joueur';

$lang['op_username']						= 'Nom du joueur';
$lang['op_old_pass']						= 'Ancien mot de passe';
$lang['op_new_pass']						= 'Nouveau mot de passe (8 caractères min.)';
$lang['op_repeat_new_pass']					= 'Nouveau mot de passe (répéter)';
$lang['op_email_adress']					= 'Adresse e-mail';
$lang['op_permanent_email_adress']			= 'Adresse e-mail permanente';
$lang['op_general_settings']				= 'Réglages générals';
$lang['op_sort_planets_by']					= 'Classer les planètes dans l\'ordre suivant :';
$lang['op_sort_kind']						= 'Ordre de classement :';
$lang['op_skin_example']					= 'Skins (ex. C:/ogame/skin/)<br> <a href="http://80.237.203.201/download/" target="_blank">télécharger</a>';
$lang['op_show_skin']						= 'Utiliser le skin';
$lang['op_deactivate_ipcheck']				= 'Désactiver la vérification d\'IP';
$lang['op_galaxy_settings']					= 'Paramétrages de la vue de la galaxie';
$lang['op_spy_probes_number']				= 'Nombre de sondes d\'espionnage';
$lang['op_toolt_data']						= 'Afficher tooltips pour';
$lang['op_seconds']							= 'secondes';
$lang['op_max_fleets_messages']				= 'Nombre maximal de messages de flotte';
$lang['op_show_ally_logo']					= 'Montrer le logo des alliances';
$lang['op_shortcut']						= 'Raccourcis';
$lang['op_show']							= 'Montrer';
$lang['op_spy']								= 'Espionner';
$lang['op_write_message']					= 'Écrire un message';
$lang['op_add_to_buddy_list']				= 'Ajouter à la liste d\'amis';
$lang['op_missile_attack']					= 'Attaquer avec les missiles';
$lang['op_send_report']						= 'Voir rapport';
$lang['op_vacation_delete_mode']			= 'Mode vacances / Effacer le compte';
$lang['op_activate_vacation_mode']			= 'Activer le mode vacances';
$lang['op_dlte_account']					= 'Effacer le compte';
$lang['op_email_adress_descrip']			= 'Cette adresse peut être changée à tout moment. L\'adresse deviendra permanente s\'il n\'y a pas de changement pendant les 7 jours qui suivent.';
$lang['op_deactivate_ipcheck_descrip']		= 'La vérification d\'IP signifie qu\'on effectuera une déconnexion de sécurité automatiquement quand il y aura un changement d\'IP ou quand 2 personnes entreront sur le même compte en utilisant différentes IPs. Activer la vérification d\'IP peut être un atout de sécurité !';
$lang['op_spy_probes_number_descrip']		= 'Nombre de sondes d\'espionnage envoyées depuis le menu Galaxie à chaque fois que vous espionnez quelqu\'un.';
$lang['op_activate_vacation_mode_descrip']	= 'Le mode vacances est là pour vous protéger pendant votre absence. Il ne peut être activé que si rien ne se construit (flotte, bâtiment ou défense), si il n\'y a rien en cours de recherche, et si aucune de vos flottes est en cours de vol. Une fois qu\'il est activé, vous êtes protégé contre de nouvelles attaques. Les attaques qui ont déjà commencé seront effectuées. Pendant le mode de vacances, la production est fixée à zéro et doit être remise manuellement à 100% après que le mode de vacances soit désactivé. Le mode de vacances a une durée minimum de deux jours et ne peut être désactivé qu\'après cette période.';
$lang['op_dlte_account_descrip']			= 'Ton compte sera complétement effacé dans 7 jours.';
$lang['op_sort_colonization']				= 'Date de colonisation';
$lang['op_sort_coords']						= 'Coordonnées';
$lang['op_sort_alpha']						= 'Ordre alphabétique';
$lang['op_sort_asc']						= 'Croissant';
$lang['op_sort_desc']						= 'Décroissant';

//----------------------------------------------------------------------------//
//BANNED
$lang['bn_no_players_banned']				= 'Pas de joueurs bannis';

$lang['bn_exists']							= 'Il y a ';
$lang['bn_players_banned']					= ' joueur/s banni/s';
$lang['bn_players_banned_list']				= 'Liste des joueurs bannis';
$lang['bn_player']							= 'Joueur';
$lang['bn_reason']							= 'Raison';
$lang['bn_from']							= 'Du';
$lang['bn_until']							= 'Jusqu\'à';
$lang['bn_by']								= 'Par';

//----------------------------------------------------------------------------//
//SYSTEM
$lang['sys_attacker_lostunits'] 			= "L'attaquant a perdu au total";
$lang['sys_defender_lostunits'] 			= "Le défenseur a perdu au total";
$lang['sys_units']							= "unités";
$lang['debree_field_1'] 					= "Un champ de débris contenant";
$lang['debree_field_2']						= "se forme dans l'orbite de cette planète.";
$lang['sys_moonproba'] 						= "La probabilitée de création d'une lune est de :";
$lang['sys_moonbuilt'] 						= "Une lune fait son apparition autour de la planète %s [%d:%d:%d] !";
$lang['sys_attack_title']    				= "Les flottes suivantes se sont affrontées le ";
$lang['sys_attack_round']					= "Round";
$lang['sys_attack_attacker_pos'] 			= "Attaquant";
$lang['sys_attack_techologies'] 			= "Armes: %d %% Bouclier: %d %% Coque: %d %% ";
$lang['sys_attack_defender_pos'] 			= "Défenseur";
$lang['sys_ship_type'] 						= "Type";
$lang['sys_ship_count'] 					= "Nombre";
$lang['sys_ship_weapon'] 					= "Armes";
$lang['sys_ship_shield'] 					= "Bouclier";
$lang['sys_ship_armour'] 					= "Coque";
$lang['sys_destroyed'] 						= "Détruit !";
$lang['fleet_attack_1'] 					= "La flotte attaquante tire avec une puissance de";
$lang['fleet_attack_2']						= "sur le défenseur. Les boucliers du défenseur absorbent";
$lang['fleet_defs_1'] 						= "La flotte défensive tire au total";
$lang['fleet_defs_2']						= "sur l'attaquant. Les boucliers de l'attaquant absorbent";
$lang['damage']								= "points de dégâts.";
$lang['sys_attacker_won'] 					= "L'attaquant a gagné la bataille !";
$lang['sys_defender_won'] 					= "Le défenseur a gagné la bataille !";
$lang['sys_both_won'] 						= "La bataille se termine par un match nul !";
$lang['sys_stealed_ressources'] 			= "Il emporte";
$lang['sys_and']							= "et";
$lang['sys_mess_tower'] 					= "Tour de contrôle";
$lang['sys_mess_attack_report'] 			= "Rapport de combat";
$lang['sys_spy_maretials'] 					= "Ressources";
$lang['sys_spy_fleet'] 						= "Flotte";
$lang['sys_spy_defenses'] 					= "Défenses";
$lang['sys_mess_qg'] 						= "Quartier général";
$lang['sys_mess_spy_report_moon']			= "(Lune)";
$lang['sys_mess_spy_report'] 				= "Rapport d\'espionnage";
$lang['sys_mess_spy_lostproba'] 			= "Probabilité de destruction de la flotte d\'espionnage : %d %% ";
$lang['sys_mess_spy_control'] 				= "Contrôle aérospatial";
$lang['sys_mess_spy_activity'] 				= "Activité d\'espionnage";
$lang['sys_mess_spy_ennemyfleet'] 			= "Une flotte ennemie de la planète";
$lang['sys_mess_spy_seen_at'] 				= "a été aperçue à proximité de votre planète";
$lang['sys_mess_spy_destroyed'] 			= "Votre flotte a été détruite !";
$lang['sys_stay_mess_stay'] 				= "Arrivée sur une planète";
$lang['sys_stay_mess_start'] 				= "Votre flotte atteint la planète";
$lang['sys_stay_mess_end'] 					= " et y livre les ressources suivantes : ";
$lang['sys_adress_planet'] 					= "[%s:%s:%s]";
$lang['sys_stay_mess_goods'] 				= "%s : %s, %s : %s, %s : %s";
$lang['sys_colo_mess_from'] 				= "Colonisation";
$lang['sys_colo_mess_report'] 				= "Rapport de colonisation";
$lang['sys_colo_defaultname'] 				= "Colonie";
$lang['sys_colo_arrival'] 					= "La flotte atteint les coordonnées ";
$lang['sys_colo_maxcolo'] 					= ", mais malheureusement la colonisation est impossible, vous ne pouvez pas avoir plus de ";
$lang['sys_colo_allisok'] 					= ", et les colons commencent à développer cette nouvelle partie de l'empire.";
$lang['sys_colo_badpos']  					= ", et les colons ont trouvé un environnement peu propice à l'extention de votre empire. Ils ont décidé de rebrousser chemin totalement dégoutés...";
$lang['sys_colo_notfree'] 					= ", et les colons n'ont pas trouvé de planète à ces coordonnées. Ils sont forcés de rebrousser chemin totalement démoralisés...";
$lang['sys_colo_planet']  					= " planètes !";
$lang['sys_expe_report'] 					= "Rapport d'expédition";
$lang['sys_recy_report'] 					= "Rapport d'exploitation";
$lang['sys_expe_blackholl_1'] 				= "La flotte a été aspirée dans un trou noir, elle a été partiellement détruite.";
$lang['sys_expe_blackholl_2'] 				= "La flotte a été aspirée dans un trou noir, elle a été entièrement détruite.";
$lang['sys_expe_nothing_1'] 				= "Vos explorateurs ont prit de magnifiques photos. Mais ils n'ont trouvé aucune ressource.";
$lang['sys_expe_nothing_2'] 				= "Vos explorateurs ont passés tout le temps imparti dans la zone choisie. Mais ils n'ont trouvé ni ressources ni planète.";
$lang['sys_expe_found_goods'] 				= "La flotte a découvert un planète non habitée !<br>Vos explorateurs ont récupérés %s de %s, %s de %s et %s de %s.";
$lang['sys_expe_found_ships'] 				= "Vos explorateurs ont trouvés des vaisseaux abandonnés en parfait état de marche. <br> Ils ont trouvés :";
$lang['sys_expe_back_home'] 				= "Votre flotte d'expédition rentre à quai.";
$lang['sys_mess_transport'] 				= "Flotte de transport";
$lang['sys_tran_mess_owner'] 				= "Une de vos flottes arrive sur %s %s. Elle livre %s unités de %s, %s unités de %s et %s unités de %s.";
$lang['sys_tran_mess_user']  				= "Une flotte alliée venant de %s %s arrive sur %s %s. Elle livre %s unités de %s, %s unités de %s et %s unités de %s.";
$lang['sys_mess_fleetback'] 				= "Retour de flotte";
$lang['sys_tran_mess_back'] 				= "Une de vos flottes rentre de %s %s. La flotte ne livre pas de ressources.";
$lang['sys_recy_gotten'] 					= "Vous avez collecté %s unités de %s et %s unités de %s.";
$lang['sys_notenough_money'] 				= "Vous ne disposez pas de suffisement de ressources pour lancer la construction de %s. Vous disposez de %s de %s, %s de %s et %s de %s et le coût du bâtiment était de %s de %s, %s de %s et %s de %s.";
$lang['sys_nomore_level'] 					= "Vous tentez de détruire un bâtiment que vous ne possédez plus ( %s ).";
$lang['sys_buildlist'] 						= "Liste de construction";
$lang['sys_buildlist_fail'] 				= "Construction impossible";
$lang['sys_gain'] 							= "Gains";
$lang['sys_fleet_won'] 						= "Votre flotte retourne à la planète %s %s et y restitue les ressources suivantes : %s de %s, %s de %s et %s de %s.";
$lang['sys_perte_attaquant'] 				= "Perte Attaquant";
$lang['sys_perte_defenseur'] 				= "Perte Défenseur";
$lang['sys_debris'] 						= "Débris";
$lang['sys_destruc_title']    				= "Tentative de destruction lunaire de %s :";
$lang['sys_mess_destruc_report'] 			= "Rapport de destruction";
$lang['sys_destruc_lune'] 					= "La probabilitée de destruction de la lune est de : %d %% ";
$lang['sys_destruc_rip'] 					= "La probabilitée de destruction de la flotte d'étoiles de la mort est de : %d %% ";
$lang['sys_destruc_stop'] 					= "Le défenseur a réussi a bloqué la tentative de destruction de lune";
$lang['sys_destruc_mess1'] 					= "Cette flotte d'étoiles de la mort concentre leurs chocs de gravitons alternants sur cette lune";
$lang['sys_destruc_mess'] 					= "Une flotte de la planète %s [%d:%d:%d] atteint la lune de la planète en[%d:%d:%d]";
$lang['sys_destruc_echec'] 					= ". Des tremblements secouent la surface de la lune. Mais quelque chose se passe mal. Les canons de graviton secouent la flotte d'étoiles de la mort, il y a retour fatal. Hélas ! La flotte d'étoiles de la mort explose en millions de fragments ! L'explosion détruit entièrement la flotte.";
$lang['sys_destruc_reussi'] 				= ", provoquant un tremblement puis un déchirement total de celle-ci. Tous les bâtiments sont détruits - Mission accomplie ! La lune est détruite ! La flotte rentre à la planète de départ.";
$lang['sys_destruc_null'] 					= ", visiblement la flotte ne développe pas la puissance nécessaire - Échec de la mission ! La flotte rentre à la planète de départ.";
$lang['sys_the']							= " le ";
$lang['sys_stay_mess_back']         		= "Une de vos flottes rentre de ";
$lang['sys_stay_mess_bend']         		= " et rapporte ";

//----------------------------------------------------------------------------//
//class.CheckSession.php
$lang['ccs_multiple_users']					= 'Erreur de cookie ! Plusieurs utilisateurs utilisent ce nom ! Vous devez supprimer vos cookies. En cas de problèmes, veuillez contacter l\'admininistrateur.';
$lang['ccs_other_user']						= 'Erreur de cookie ! Votre cookie ne correspond pas au compte ! Vous devez supprimer vos cookies. En cas de problèmes, veuillez contacter l\'admininistrateur.';
$lang['css_different_password']				= 'Erreur de cookie ! Erreur de session, vous devez vous reconnecter ! Vous devez supprimer vos cookies. En cas de problèmes, veuillez contacter l\'admininistrateur.';
$lang['css_account_banned_message']			= 'VOTRE COMPTE A ÉTÉ BANNI !';
$lang['css_account_banned_expire']			= 'Fin du bannissement : ';

//----------------------------------------------------------------------------//
//class.debug.php
$lang['cdg_mysql_not_available']			= 'MySQL n\'est pas disponible pour le moment...';
$lang['cdg_error_message']					= 'Erreur, veuillez contacter l\'administrateur. Erreur n°:';
$lang['cdg_fatal_error']					= 'ERREUR FATALE';

//----------------------------------------------------------------------------//
//class.FlyingFleetsTable.php
$lang['cff_no_fleet_data']					= 'Aucune donnée de flotte';
$lang['cff_aproaching']						= 'Ils approchent de ';
$lang['cff_ships']							= ' vaisseaux';
$lang['cff_from_the_planet']				= 'de la planète ';
$lang['cff_from_the_moon']					= 'de la lune ';
$lang['cff_the_planet']						= 'la planète ';
$lang['cff_debris_field']					= 'le champ de débris ';
$lang['cff_to_the_moon']					= 'à la lune ';
$lang['cff_the_position']					= 'la position ';
$lang['cff_to_the_planet']					= ' à la planète ';
$lang['cff_the_moon']						= ' la lune ';
$lang['cff_from_planet']					= 'de la planète ';
$lang['cff_from_debris_field']				= 'du champ de débris ';
$lang['cff_from_the_moon']					= 'de la lune ';
$lang['cff_from_position']					= 'de la position ';
$lang['cff_missile_attack']					= 'Attaque aux missiles';
$lang['cff_from']							= ' de ';
$lang['cff_to']								= ' à ';
$lang['cff_one_of_your']					= 'Une de vos ';
$lang['cff_a']								= 'Une ';
$lang['cff_of']								= ' de ';
$lang['cff_goes']							= ' venant ';
$lang['cff_toward']							= ' atteint ';
$lang['cff_with_the_mission_of']			= '. Elle a pour mission: ';
$lang['cff_to_explore']						= ' explore ';
$lang['cff_comming_back']					= ' rentre ';
$lang['cff_back']							= 'De retour';
$lang['cff_to_destination']					= 'Intitulé de la destination';
$lang['cff_flotte'] 						= ' flottes';

//----------------------------------------------------------------------------//
// EXTRA LANGUAGE FUNCTIONS
$lang['fcm_moon']							= 'Lune';
$lang['fcp_colony']							= 'Colonie';
$lang['fgp_require']						= 'Ressources nécessaires: ';
$lang['fgf_time']							= 'Durée de construction: ';

//----------------------------------------------------------------------------//
// CombatReport.php

$lang['cr_lost_contact']					= 'Le contact a été perdu avec la flotte d\'attaque.';
$lang['cr_first_round']						= '(La flotte a été détruite au premier tour)';
$lang['cr_type']							= 'Type';
$lang['cr_total']							= 'Total';
$lang['cr_weapons']							= 'Armes';
$lang['cr_shields']							= 'Bouclier';
$lang['cr_armor']							= 'Coque';
$lang['cr_destroyed']						= 'Détruit !';

//----------------------------------------------------------------------------//
// FleetAjax.php
$lang['fa_not_enough_probes']				= 'Erreur, pas suffisamment de sondes.';
$lang['fa_galaxy_not_exist']				= 'Erreur, la galaxie n\'existe pas.';
$lang['fa_system_not_exist']				= 'Erreur, le système n\'existe pas.';
$lang['fa_planet_not_exist']				= 'Erreur, la planète n\'existe pas.';
$lang['fa_not_enough_fuel']					= 'Erreur, vous n\'avez pas assez de carburant.';
$lang['fa_no_more_slots']					= 'Erreur, vous avez pas d\'autres slots de flotte disponible.';
$lang['fa_no_recyclers']					= 'Erreur, les recycleurs ne sont pas disponibles.';
$lang['fa_mission_not_available']			= 'Erreur, la mission n\'est pas disponible.';
$lang['fa_no_ships']						= 'Erreur, pas de vaisseaux disponibles.';
$lang['fa_vacation_mode']					= 'Erreur, le joueur est en mode vacances.';
$lang['fa_week_player']						= 'Erreur, le joueur est trop faible.';
$lang['fa_strong_player']					= 'Erreur, le joueur est trop fort.';
$lang['fa_not_spy_yourself']				= 'Erreur, vous ne pouvez pas vous espionner vous-même.';
$lang['fa_not_attack_yourself']				= 'Erreur, vous ne pouvez pas vous attaquer vous-même.';
$lang['fa_action_not_allowed']				= 'Erreur, action non permis.';
$lang['fa_vacation_mode_current']			= 'Erreur, vous êtes en mode vacances.';
$lang['fa_sending']							= 'Envoi';

//----------------------------------------------------------------------------//
// MissilesAjax.php
$lang['ma_silo_level']						= 'Le silo doit être au moins au niveau 4.';
$lang['ma_impulse_drive_required']			= 'Vous devez avoir la recherche Réacteur à impulsion.';
$lang['ma_not_send_other_galaxy']			= 'Vous ne pouvez pas envoyer des missiles vers une autre galaxie.';
$lang['ma_planet_doesnt_exists']			= 'Le monde objectif n\'existe pas.';
$lang['ma_cant_send']						= 'Vous ne pouvez pas envoyer ';
$lang['ma_missile']							= ' missiles, vous en avez seulement ';
$lang['ma_wrong_target']					= 'Mauvaise cible';
$lang['ma_no_missiles']						= 'Il n\'y a pas de missiles interplanétaire disponibles.';
$lang['ma_add_missile_number']				= 'Entrer le nombre de missiles à envoyer';
$lang['ma_misil_launcher']					= 'Lanceur de missiles';
$lang['ma_small_laser']						= 'Artillerie laser légère';
$lang['ma_big_laser']						= 'Artillerie laser lourde';
$lang['ma_gauss_canyon']					= 'Canon de Gauss';
$lang['ma_ionic_canyon']					= 'Artillerie à ions';
$lang['ma_buster_canyon']					= 'Lanceur de plasma';
$lang['ma_small_protection_shield']			= 'Petit bouclier';
$lang['ma_big_protection_shield']			= 'Grand bouclier';
$lang['ma_all']								= 'Tout';
$lang['ma_missiles_sended']					= ' missiles interplanétaires ont été envoyés. Objectif principal: ';
$lang["ma_all_destroyed"]  					='Tous les missiles interplanétaires ont été détruits par des missiles d\'interception.';
$lang['ma_planet_without_defens']  			='Planète sans défenses';
$lang['ma_some_destroyed']   				=' a été détruit par des missiles d\interception.';
$lang['ma_missile_attack']					='';
$lang['ma_missile_string']					='L\'attaque de missiles (%1%) depuis %2% arrive sur la planète %3% <br><br>';


//----------------------------------------------------------------------------//
// RULES
$lang['respectrules'] = 'Ces r&egrave;gles sont &agrave; respecter, tout infraction &agrave; ces r&egrave;gles sera sanctionn&eacute;e par un bannissement temporaire ou d&eacute;finitif !';
$lang['rules']        = 'R&egrave;glement';

$lang['Account']      = 'I.    Comptes';
$lang['MultiAccount'] = 'II.   Multicomptes';
$lang['Sitting']      = 'III.  Surveillance du compte d\'autrui (Sitting)';
$lang['Trade']        = 'IV.   Echange de comptes';
$lang['Bash']         = 'V.    Bash';
$lang['Push']         = 'VI.   Push';
$lang['Bugusing']     = 'VII.  Bugusing';
$lang['MailIngame']   = 'VIII. Abus de la fonction de signalement de messages ingame';
$lang['OutXnova']     = 'IX.   Menaces sortant du cadre du serveur';
$lang['Spam']         = 'X.    Spam, insultes et contenus offensants';

$lang['AccountText']  = 'Un joueur a le droit de jouer sur un seul compte par univers.';
$lang['AccountText2'] = 'Un compte n\'a le droit d\'&ecirc;tre jou&eacute; que par une seule personne.';

$lang['MultiAccountText']  = 'Jouer plus d\'un compte par univers est strictement interdit.';
$lang['MultiAccountText2'] = 'Si 2 joueurs ou plus se partagent la m&ecirc;me adresse IP (membres d\'une m&ecirc;me famille, couples, &eacute;coles etc), il ne doit y avoir aucun autre point commun entre ces comptes (faire partie de la m&ecirc;me alliance est tol&eacute;r&eacute;), ceci pouvant &ecirc;tre consid&eacute;r&eacute; comme du multi compte et &eacute;tant passible d\'un blocage.';
$lang['MultiAccountText3'] = 'Toute interaction entre comptes sur la m&ecirc;me IP est interdite.';

$lang['SittingText']  = 'Le sitting de compte est assujetti au respect des r&egrave;gles suivantes:';
$lang['SittingText2'] = 'Un compte ne peut &ecirc;tre sitt&eacute; que pour 12 heures cons&eacute;cutives.';
$lang['SittingText3'] = 'L\'op&eacute;rateur de l\'univers doit imp&eacute;rativement &ecirc;tre inform&eacute; par mail de ce sitting.';
$lang['SittingText4'] = 'Lorsque le compte est sitt&eacute;, le surveillant a le droit d\'activer des constructions de b&acirc;timents ou de lancer des recherches avec les ressources pr&eacute;sentes sur la plan&egrave;te. Le transfert de ressources depuis d\'autres plan&egrave;tes ou lunes du compte est strictement interdit.';
$lang['SittingText5'] = 'Avant que le compte puisse &ecirc;tre resitt&eacute;, il doit y avoir login du propri&eacute;taire du compte.';
$lang['SittingText6'] = 'Le sitting suivant ne peut avoir lieu que 7 jours apr&egrave;s le login du propri&eacute;taire du compte.';
$lang['SittingText7'] = 'Un compte ne peut changer de propri&eacute;taire que tous les 3 mois (sans exception aucune possible !).';
$lang['SittingText8'] = 'Le surveillant du compte ne peut sitter un compte sur cet univers dans les 7 jours qui suivent un sitting.';
$lang['SittingText9'] = 'Le sitting ne doit pas &ecirc;tre utilis&eacute; pour se procurer un avantage (par ex. utiliser une phalange ou la flotte).';
$lang['SittingText10'] = 'Ce qui est strictement interdit:';
$lang['SittingText11'] = 'Aucun mouvement de flotte n\'est autoris&eacute; pendant que le compte est sitt&eacute; (aucune flotte ne doit &ecirc;tre en vol &agrave; ce moment l&agrave;). Cependant, si une flotte est en train de se faire attquer, il est permis de l\'envoyer en mode "Transport" ou "Stationner" vers une autre plan&egrave;te ou lune de ce compte pour esquiver l\'attaque.';
$lang['SittingText12'] = 'Le sitting de compte est interdit pendant les trois premi&egrave;res semaines &agrave; compter de la date de d&eacute;marrage d\'un univers.';
$lang['SittingText13'] = 'Pendant un sitting, seul le sitteur est autoris&eacute; &agrave; se logger.';
$lang['SittingText14'] = 'Il est interdit de se faire sitter par plusieurs joueurs pendant la p&eacute;riode de 12 heures.';
$lang['SittingText15'] = 'Il est interdit de sitter un autre compte.';

$lang['TradeText'] = 'Un compte appartient &agrave; la personne &agrave; qui appartient l\'adresse permanente qui lui est associ&eacute;. Si un &eacute;change de comptes a lieu sans passer par un op&eacute;rateur d\'univers, ceci s\'effectue aux risques et p&eacute;rils des propri&eacute;taires respectifs, aucune plainte ou demande ne sera trait&eacute;e si elle ne provient pas de l\'adresse mail permanente du compte concern&eacute;.';
$lang['TradeText2'] = 'Les op&eacute;rateurs de jeu ont non seulement la possibilit&eacute; d\'effectuer tr&egrave;s facilement un &eacute;change de comptes, ils peuvent aussi par ce moyen emp&ecirc;cher que le compte soit vol&eacute; au cours de l\'&eacute;change.';

$lang['BashText'] = 'Attaquer une plan&egrave;te ou une lune plus de 6 fois en l\'espace de 24 heures cons&eacute;cutives est consid&eacute;r&eacute; comme du bash et par cons&eacute;quent interdit.';
$lang['BashText2'] = 'Attaquer une lune en mode "Destruction" compte dans le cadre de cette limite de 6 attaques.';
$lang['exception'] = 'Exceptions:';
$lang['BashExepText'] = 'Le bash n\'est autoris&eacute; que si les alliances impliqu&eacute;es sont en guerre (cette guerre doit &ecirc;tre d&eacute;clar&eacute;e dans la section correspondante du forum officiel).';
$lang['BashExepText2'] = 'Les flottes attaquantes compl&egrave;tement d&eacute;truites lors d\'une attaque et les combats interrompus au bout d\'un tour par le bug du match nul ne sont pas prises en compte dans le calcul des 6 attaques.';
$lang['BashExepText3'] = 'Les attaques par missiles interplan&eacute;taires ne sont pas limit&eacute;es et ne sont pas prises en compte dans le calcul des 6 attaques.';

$lang['PushText'] = 'Le push se d&eacute;finit par le transfert volontaire de ressources sous quelque forme que ce soit d\'un joueur vers un joueur mieux class&eacute; que lui sans aucune contrepartie. Ceci est aussi valable lorsqu\'un joueur mieux class&eacute; vous fait du chantage.';
$lang['PushText2'] = 'Contrairement au bash, il n\'y a aucune exception o&ugrave; le push est autoris&eacute;:';
$lang['PushText3'] = 'Si un joueur plus faible que vous vous envoie des ressources sans que vous ne lui ayez rien demand&eacute;, veuillez lui renvoyer ou l\'envoyer &agrave; un des op&eacute;rateurs de jeu. Vous ne pouvez pas garder ces ressources!';
$lang['PushText4'] = 'Le chantage n\'est pas autoris&eacute;.';
$lang['PushText5'] = 'Les &eacute;changes de ressources doivent &ecirc;tre ex&eacute;cut&eacute;s sous 48 heures.';
$lang['exemple'] = 'Exemples (les infractions ne se limitent pas aux cas cit&eacute;s ci-dessous):';
$lang['PushEx'] = 'Un joueur envoie des ressources &agrave; un joueur mieux class&eacute; que lui.';
$lang['PushEx2'] = 'Une flotte suicide envoy&eacute;e vers un joueur plus fort dans le seul but que ce dernier puisse recycler le champ de d&eacute;bris.';
$lang['PushEx3'] = 'Transporter des ressources vers une plan&egrave;te pour les mettre &agrave; disposition d\'une attaque d\'un joueur plus fort inform&eacute; de cette manoeuvre.';
$lang['recyclage'] = 'Aide au recyclage:';
$lang['PushRec'] = 'Apr&egrave;s avoir aid&eacute; un joueur &agrave; collecter un champ de d&eacute;bris, vous &ecirc;tes autoris&eacute; &agrave; transf&eacute;rer ces ressources au joueur attaquant mieux class&eacute;, cet envoi devant &ecirc;tre obligatoirement accompagn&eacute; d\'un mail d\'information &agrave; votre op&eacute;rateur d\'univers.';
$lang['mercenariat'] = 'Mercenariats:';
$lang['PushMer'] = 'Les mercenariats doivent &ecirc;tre d&eacute;clar&eacute;s sur le forum officiel dans la section correspondante pour &ecirc;tre approuv&eacute;s. Les mercenariats non d&eacute;clar&eacute;s sont passibles de bannissement pour push.';
$lang['PushMer2'] = 'La r&eacute;compense ne peut-&ecirc;tre vers&eacute;e que lorsque la cible a &eacute;t&eacute; d&eacute;truite.';

$lang['BugusingText'] = 'Utiliser un bug &agrave; son avantage est strictement interdit.';
$lang['BugusingText2'] = 'Un joueur qui trouve un bug est pri&eacute; de le signaler imm&eacute;diatement au staff (par ex. via le forum, l\'IRC ou les mails).';
$lang['BugusingText3'] = 'Ne pas signaler un bug trouv&eacute; est passible de bannissement.';
$lang['BugusingText4'] = 'Toute m&eacute;thode de jeu visant &agrave; rendre le compte d\'un adversaire lent ou injouable est absolument interdite.';

$lang['MailIngameText'] = 'Utiliser le bouton Signaler pour signaler un message qui ne contient pas d\'insultes ou qui n\'enfreint pas les r&egrave;gles du jeu est interdit.';

$lang['OutText']        = 'Il est interdit de menacer quelqu\'un de cons&eacute;quences dans la vie r&eacute;elle, ceci &eacute;tant valable pour le jeu, le forum et l\'IRC.';

$lang['SpamText']       = 'Le spam, les insultes et les messages &agrave; contenus offensants sont interdits, de m&ecirc;me que tout contenu x&eacute;nophobe, antis&eacute;mite ou raciste.';
?>

<?php
//---------------	GENERAL	------------------------//
$lang['404_page']			= 'Vous n\'avez pas les autorisations nécéssaires';
$lang['adm_cp_title']		= 'Administration control Panel';
$lang['adm_cp_index']		= 'Accueil';
$lang['mu_moderation_page']	= 'Permissions';
$lang['adm_cp_logout']		= 'Quitter';
$lang['lang_key']			= 'fr';

//	MISCELÁNEO - MISCELLANEOUS - DIVERS
$lang['ad_number']			= 'Nº';
$lang['only_numbers']		= 'Vous pouvez entrer que des nombres !';
$lang['select_option']		= 'Sélectionner...';

// 	RANGO - RANK - RANG
$lang['rank'][0]	= 'Joueur';
$lang['rank'][1]	= 'Modérateur';
$lang['rank'][2]	= 'Opérateur';
$lang['rank'][3]	= 'Administrateur';

// 	TIEMPO - TIME - TEMPS
$lang['time_days']		= 'Jours';
$lang['time_hours']		= 'Heures';
$lang['time_minutes']	= 'Minutes';
$lang['time_seconds']	= 'Secondes';
//###########################################################//


//---------------	¿SI O NO? - YES OR NO? - OUI OU NON	------------------------//
$lang['one_is_yes'][1]	= 'Oui';
$lang['one_is_yes'][0]	= 'Non';

$lang['one_is_no'][1]	= 'Non';
$lang['one_is_no'][0]	= 'Oui';
//###########################################################//


//---------------	BOTONES - BUTTONS - BOUTONS	------------------------//
$lang['button_submit']		= 'Envoyer';
$lang['button_add']			= 'Ajouter';
$lang['button_delete']		= 'Supprimer';
$lang['button_filter']		= 'Filtre';
$lang['button_deselect']	= 'Désélectionner';
$lang['button_reset']		= 'Recommencer';
$lang['button_des_se']		= 'Sélectionner/Désélectionner tout';
//###########################################################//


//---------------	ID's	------------------------//
$lang['input_id']			= 'ID';
$lang['input_id_user']		= 'ID JOUEUR';
$lang['input_id_planet']	= 'ID PLANÈTE';
$lang['input_id_moon']		= 'ID LUNE';
$lang['input_id_p_m']		= 'ID PLANÈTE ou LUNE';
$lang['input_id_ally']		= 'ID ALLIANCE';
//###########################################################//


//---------------	RECURSOS - RESOURCES - RESSOURCES	------------------------//
$lang['resources_title']	= 'Ressources';
$lang['count_res']			= 'Ressources total';
$lang['metal']				= 'Métal';
$lang['crystal']			= 'Cristal';
$lang['deuterium']			= 'Deutérium';
$lang['energy']				= 'Énergie';
$lang['darkmatter']			= 'Anti-matière';
//###########################################################//


//---------------	NAVES - SHIPS - VAISSEAUX	------------------------//
$lang['ships_title']		= 'Vaisseaux';
$lang['ships_count']		= 'Vaisseaux total';
$lang['small_ship_cargo']	= 'Petit transporteur';
$lang['big_ship_cargo']		= 'Grand transporteur';
$lang['light_hunter']		= 'Chasseur léger';
$lang['heavy_hunter']		= 'Chasseur lourd';
$lang['crusher']			= 'Croiseur';
$lang['battle_ship']		= 'Vaisseau de bataille';
$lang['colonizer']			= 'Vaisseau de colonisation';
$lang['recycler']			= 'Recycleur';
$lang['spy_sonde']			= 'Sonde espionnage';
$lang['bomber_ship']		= 'Bombardier';
$lang['solar_satelit']		= 'Satellite solaire';
$lang['destructor']			= 'Destructeur';
$lang['dearth_star']		= 'Étoile de la mort';
$lang['battleship']			= 'Traqueur';
//###########################################################//


//---------------	DEFENSAS - DEFENSES - DÉFENSES	------------------------//
$lang['defenses_title']				= 'Défenses';
$lang['defenses_count']				= 'Défenses total';
$lang['misil_launcher']				= 'Lanceur de missiles';
$lang['small_laser']				= 'Artillerie laser légère';
$lang['big_laser']					= 'Artillerie laser lourde';
$lang['gauss_canyon']				= 'Canon de Gauss';
$lang['ionic_canyon']				= 'Artillerie à ions';
$lang['buster_canyon']				= 'Lanceur de plasma';
$lang['small_protection_shield']	= 'Petit bouclier';
$lang['big_protection_shield']		= 'Grand bouclier';
$lang['interceptor_misil']			= 'Missile d\'interception';
$lang['interplanetary_misil']		= 'Missile Interplanétaire';
//###########################################################//


//---------------	EDIFICIOS - BUILDINGS - BÂTIMENTS	------------------------//
$lang['buildings_title']		= 'Bâtiments';
$lang['moon_build']				= 'Bâtiments lunaires';
$lang['buildings_count']		= 'Bâtiments total';
$lang['metal_mine']				= 'Mine de métal';
$lang['crystal_mine']			= 'Mine de cristal';
$lang['deuterium_sintetizer']	= 'Synthétiseur de deutérium';
$lang['solar_plant']			= 'Centrale électrique solaire';
$lang['fusion_plant']			= 'Centrale électrique de fusion';
$lang['robot_factory']			= 'Usine de robots';
$lang['nano_factory']			= 'Usine de nanites';
$lang['shipyard']				= 'Chantier spatial';
$lang['metal_store']			= 'Hangar de métal';
$lang['crystal_store']			= 'Hangar de cristal';
$lang['deuterium_store']		= 'Réservoir de deutérium';
$lang['laboratory']				= 'Laboratoire de recherche';
$lang['terraformer']			= 'Terraformeur';
$lang['ally_deposit']			= 'Dépôt de ravitaillement';
$lang['silo']					= 'Silo de missiles';
$lang['moonbases']				= 'Base lunaire';
$lang['phalanx']				= 'Phalange de capteur';
$lang['cuantic']				= 'Porte de saut spatial';
//###########################################################//


//---------------	INVESTIGACIONES - RESEARCHS - RECHERCHES	------------------------//
$lang['researchs_title']		= 'Recherches';
$lang['researchs_count']		= 'Recherches total';
$lang['spy_tech']				= 'Technologie Espionnage';
$lang['computer_tech']			= 'Technologie Ordinateur';
$lang['military_tech']			= 'Technologie Armes';
$lang['defence_tech']			= 'Technologie Protection des vaisseaux spatiaux';
$lang['shield_tech']			= 'Technologie Bouclier';
$lang['energy_tech']			= 'Technologie Énergie';
$lang['hyperspace_tech']		= 'Technologie Hyperespace';
$lang['combustion_tech']		= 'Réacteur à combustion';
$lang['impulse_motor_tech']		= 'Réacteur à impulsion';
$lang['hyperspace_motor_tech']	= 'Propulsion hyperespace';
$lang['laser_tech']				= 'Technologie Laser';
$lang['ionic_tech']				= 'Technologie Ions';
$lang['buster_tech']			= 'Technologie Plasma';
$lang['intergalactic_tech']		= 'Réseau de recherches intergalactique';
$lang['expedition_tech']		= 'Technologie Expéditions';
$lang['graviton_tech']			= 'Technologie Graviton';
//###########################################################//


//---------------	OFICIALES - OFFICERS - OFFICIERS	------------------------//
$lang['officiers_title']		= 'Officiers';
$lang['officiers_count']		= 'Officiers total';
$lang['geologist']				= 'Géologue';
$lang['admiral']				= 'Amiral';
$lang['engineer']				= 'Ingénieur';
$lang['technocrat']				= 'Technocrate';
$lang['spy']					= 'Espion';
$lang['constructor']			= 'Constructeur';
$lang['scientific']				= 'Scientifique';
$lang['commander']				= 'Commandant';
$lang['storer']					= 'Stockeur';
$lang['defender']				= 'Défenseur';
$lang['destroyer']				= 'Destructeur';
$lang['general']				= 'Général';
$lang['protector']				= 'Protecteur';
$lang['conqueror']				= 'Conquérant';
$lang['emperor']				= 'Empereur';
//###########################################################//


//---------------	CONSULTAS SQL - SQL QUERIES - REQUÊTES SQL	------------------------//
$lang['qe_title_menu']	= 'Requêtes SQL';
$lang['qe_execute']		= 'Exécuter des requêtes SQL';
$lang['qe_succes']		= 'Opération effectuée avec succès';
$lang['qe_note']		= 'Note: insérer une requête SQL.';
//###########################################################//


//---------------	EDITOR	------------------------//
// GLOBAL - MENU
$lang['ad_forgiven_id']			= 'Vous devez entrer un ID !';
$lang['ad_back_to_menu']		= 'Retour au menu principal';
$lang['ad_editor_title']		= 'Éditeur de comptes';
$lang['ad_editor_buildings']	= 'Bâtiments';
$lang['ad_editor_ships']		= 'Vaisseaux';
$lang['ad_editor_defenses']		= 'Défenses';
$lang['ad_editor_researchs']	= 'Recherches';
$lang['ad_editor_officiers']	= 'Officiers';
$lang['ad_editor_personal']		= 'Données personnelles';
$lang['ad_editor_planets']		= 'Planètes et lunes';
$lang['ad_editor_resources']	= 'Ressources';
$lang['ad_editor_alliances']	= 'Alliance';
$lang['ad_editor_authlevels']	= 'Gérer les autorisations';

//	INVESTIGACIONES - RESEARCHS - RECHERCHES
$lang['ad_add_succes']		= 'Recherche(s) ajoutée(s) avec succès';
$lang['ad_delete_succes']	= 'Recherche(s) supprimée(s) avec succès';
$lang['ad_research_title']	= 'Éditer recherches';

//	OFICIALES - OFFICERS - OFFICIERS
$lang['ad_offi_title']			= 'Éditer officiers';
$lang['ad_offi_succes_add']		= 'Officier(s) ajouté(s) avec succès';
$lang['ad_offi_succes_delete']	= 'Officier(s) supprimé(s) avec succès';

//	RECURSOS - RESOURCES - RESSOURCES
$lang['ad_add_sucess']		= 'Ressources ajoutées avec succès';
$lang['ad_delete_sucess']	= 'Ressources supprimées avec succès';
$lang['ad_main_title']		= 'Éditer ressources et technologies';

//	NAVES - SHIPS - VAISSEAUX
$lang['ad_ships_title']			= 'Éditer vaisseaux';
$lang['ad_add_sucess_ships']	= 'Vaisseau(x) ajouté(s) avec succès';
$lang['ad_delete_sucess_ships']	= 'Vaisseau(x) supprimé(s) avec succès';

//	DEFENSAS - DEFENSES - DÉFENSES
$lang['ad_defenses_title']			= 'Éditer défenses';
$lang['ad_add_defenses_succes']		= 'Défense(s) ajoutée(s) avec succès';
$lang['ad_delete_defenses_succes']	= 'Défense(s) supprimée(s) avec succès';

//	EDIFICIOS - BUILDINGS - BÂTIMENTS
$lang['ad_buildings_title']	= 'Éditer bâtiments';
$lang['ad_planet_id']		= 'ID Planète ou Lune';
$lang['ad_levels']			= 'Niveaux total';
$lang['ad_add_succes']		= 'Bâtiment(s) ajouté(s) avec succès';
$lang['ad_delete_succes']	= 'Bâtiment(s) supprimé(s) avec succès';
$lang['ad_error_moon_only']	= 'Les bâtiments lunaires ne peuvent être ajoutés que dans les lunes !';

// DATOS PERSONALES - PERSONAL DATA - DONNÉES PERSONNELLES
$lang['ad_personal_title']		= 'Éditer les données personnelles';
$lang['ad_personal_name']		= 'Nom';
$lang['ad_personal_email']		= 'E-mail';
$lang['ad_personal_email2']		= 'E-mail permanent';
$lang['ad_personal_pass']		= 'Mot de passe';
$lang['ad_personal_succes']		= 'Paramètres modifiés avec succès';
$lang['ad_personal_vacat']		= 'Vacances?';

//	ALIANZA - ALLIANCES
$lang['ad_ally_title']		= 'Éditer alliances';
$lang['ad_ally_change_id']	= 'Changer le leader de l\'alliance';
$lang['ad_ally_name']		= 'Éditer le nom';
$lang['ad_ally_tag']		= 'Éditer le TAG';
$lang['ad_ally_text1']		= 'Texte externe';
$lang['ad_ally_text2']		= 'Texte interne';
$lang['ad_ally_text3']		= 'Texte de candidature';
$lang['ad_ally_delete']		= 'Supprimer l\'alliance';
$lang['ad_ally_delete_u']	= 'Supprimer le compte';
$lang['ad_ally_user_id']	= '(Entrer l\'ID du joueur)';
$lang['ad_ally_succes']		= 'Opération effectuée avec succès';
$lang['ad_ally_not_exist3']	= 'Le joueur n\'existe pas ou n\'est pas membre de cette alliance !';
$lang['ad_ally_not_exist']	= 'L\'alliance n\'existe pas !';
$lang['ad_ally_not_exist2']	= 'Le joueur n\`existe pas !';

//	PLANETAS Y LUNAS - PLANETS AND MOONS - PLANETES ET LUNES
$lang['ad_pla_title']			= 'Éditer les planètes et/ou lunes';
$lang['ad_pla_edit_name']		= 'Éditer le nom';
$lang['ad_pla_change_id']		= 'Changer le propriétaire (Entrer l\'ID du joueur)';
$lang['ad_pla_edit_diameter']	= 'Éditer le diamètre';
$lang['ad_pla_edit_fields']		= 'Éditer les cases';
$lang['ad_pla_delete_b']		= 'Supprimer tous les bâtiments';
$lang['ad_pla_delete_s']		= 'Supprimer tous les vaisseaux';
$lang['ad_pla_delete_d']		= 'Supprimer tous les défenses';
$lang['ad_pla_delete_hd']		= 'Supprimer la file d\'attente du hangar et de la défense';
$lang['ad_pla_delete_cb']		= 'Supprimer la file d\'attente des bâtiments';
$lang['ad_pla_delete_planet']	= 'Supprimer la planète';
$lang['ad_pla_title_a']			= 'Si vous possédez une lune, elle sera changée de propriétaire aussi';
$lang['ad_pla_title_l']			= 'Si vous possédez une lune, sa position sera modifiée aussi';
$lang['ad_pla_change_p']		= 'Changer la position de la planète';
$lang['ad_pla_change_pp']		= 'Accent tréma(¨) pour rendre le changement de position';
$lang['ad_pla_succes']			= 'Modifications apportées avec succès';
$lang['ad_pla_error_planets']	= 'Les lunes ne peuvent pas être changées de propriétaire !';
$lang['ad_pla_error_user']		= 'Le joueur n\'existe pas !';
$lang['ad_pla_error_planets2']	= 'La planète n\'existe pas !';
$lang['ad_pla_error_planets3']	= 'Les coordonnées demandées sont déjà en cours d\'utilisation !';
$lang['ad_pla_error_planets4']	= 'Vous ne pouvez pas déplacer la lune vers une planète qui en possède déjà une !';
$lang['ad_pla_error_planets5']	= 'Les coordonnées sélectionnées doivent avoir une planète !';
$lang['ad_pla_delete_planet_s']	= 'Planète supprimée avec succès';
//###########################################################//


//---------------	PANEL DE SUSPENSIÓN - BAN PANEL	------------------------//
$lang['bo_the_player']			= 'Le joueur ';
$lang['bo_the_player2']			= 'Le joueur ';
$lang['bo_banned']				= ' a été banni correctement.';
$lang['bo_unbanned']			= ' a été levé de sa suspension correctement.';
$lang['bo_username']			= 'Nom du joueur';
$lang['bo_vacaations']			= 'Vacances?';
$lang['bo_reason']				= 'Raison du bannissement';
$lang['bo_time']				= 'Temps de bannissement';
$lang['bo_vacation_mode']		= 'Mode vacances';
$lang['bo_ban_player']			= 'Suspendre/Éditer bannissement';
$lang['bo_unban_player']		= 'Liste des bannis';
$lang['bo_user_doesnt_exist']	= 'Le joueur n\'existe pas !';
$lang['bo_user_select']			= 'Sélectionner...';
$lang['bo_select_title']		= 'Voir les noms commençant par';
$lang['bo_suspended_panel']		= 'Panel de bannissement';
$lang['bo_bbb_go_back']			= '[ Retour ]';
$lang['bo_bbb_go_act']			= '[ Actualiser ]';
$lang['bo_bbb_title_1']			= 'Système de bannissement';
$lang['bo_bbb_title_2']			= 'Réglage de la date à laquelle le bannissement finira';
$lang['bo_bbb_title_3']			= 'Système de bannissement<br><font color=red>Note: ce joueur est déjà banni';
$lang['bo_bbb_title_4']			= 'Pour soustraire des jours, heures, etc. ajouter le signe moins ( - )avant le nombre, Exemple: -5 ';
$lang['bo_bbb_title_5']			= 'Banni jusqu\'à';
$lang['bo_bbb_title_6']			= 'Changer la date';
$lang['bo_characters_1']		= 'Caractères disponibles: ';
$lang['bo_characters_suus']		= '&nbsp; (Banni)';
$lang['bo_order_username']		= '[Classer par nom]';
$lang['bo_order_id']			= '[Classer par ID]';
$lang['bo_order_banned']		= '[Classer par banni]';
$lang['bo_total_users']			= 'Nombre total de comptes: ';
$lang['bo_total_banneds']		= 'Comptes bannis: ';
//###########################################################//


//---------------	ENCRIPTADOR MD5 - ENCRIPTER MD5	------------------------//
$lang['et_md5_encripter']	= 'Crypteur de clé (MD5)';
$lang['et_pass']			= 'Mot de passe';
$lang['et_result']			= 'Résultat';
$lang['et_encript']			= 'Crypter';
//###########################################################//


//---------------	CONFIGURACIÓN DE ESTADÍSTICAS - STATS CONFIG	------------------------//
$lang['cs_title']						= 'Réglage des paramètres des statistiques';
$lang['cs_point_per_resources_used']	= 'Valeur de 1 point statique';
$lang['cs_resources']					= 'ressources';
$lang['cs_users_per_block']				= 'Joueurs par bloc (min. 10)';
$lang['cs_fleets_on_block']				= 'Mise à jour des flottes dans un bloc?';
$lang['cs_time_between_updates']		= 'Délai entre les mises à jour des statistiques';
$lang['cs_minutes']						= 'minutes';
$lang['cs_points_to_zero']				= 'L\'administration peut apparaître au classement?';
$lang['cs_access_lvl']					= 'Jusqu\'à quel place peut-on apparaître au classement?';
$lang['cs_save_changes']				= 'Sauvegarder les changements';
$lang['cs_timeact_1']					= 'La dernière mise à jour était le: ';
//###########################################################//


//---------------	LISTA DE ERRORES - ERROR LIST - LISTE DES ERREURS	------------------------//
$lang['er_menu']			= 'Erreurs';
$lang['er_php']				= 'Erreurs PHP';
$lang['er_sql']				= 'Erreurs SQL';
$lang['er_errors']			= 'erreur/s';
$lang['er_sql_error_list']	= 'Liste des erreurs SQL';
$lang['er_php_error_list']	= 'Liste des erreurs PHP';
$lang['er_dlte_all']		= 'Tout supprimer';
$lang['er_type']			= 'Type';
$lang['er_level']			= 'Niveau';
$lang['er_file']			= 'Fichier';
$lang['er_line']			= 'Ligne';
$lang['er_date']			= 'Date';
$lang['er_data']			= 'Erreur';
$lang['er_user']			= 'Utilisateur';
$lang['er_php_show']		= 'Erreurs à afficher';
$lang['er_public']			= 'N/A';
$lang['er_filter']			= 'Filtrer';
//###########################################################//


//---------------	LISTA DE MENSAJES - MESSAGES LIST	------------------------//
$lang['ml_message_list']		= 'Liste des messages';
$lang['ml_page']				= 'Page';
$lang['ml_type']				= 'Type';
$lang['ml_dlte_selection']		= 'Supprimer sélection';
$lang['ml_dlte_since']			= 'Supprimer depuis';
$lang['ml_dlte_since_button']	= 'Supprimer depuis';
$lang['ml_date']				= 'Date';
$lang['ml_from']				= 'De';
$lang['ml_to']					= 'À';
$lang['ml_subject']				= 'Sujet';
$lang['ml_content']				= 'Contenu';
$lang['ml_see_all_messages']	= 'Voir tous les messages';
//###########################################################//


//---------------	MENSAJE GLOBAL - GLOBAL MESSAGE	------------------------//
$lang['ma_message_sended']		= 'Votre message a été envoyé !';
$lang['ma_subject_needed']		= 'Vous devez entrer un sujet !';
$lang['ma_send_global_message']	= 'Envoyer message global';
$lang['ma_subject']				= 'Sujet';
$lang['ma_characters']			= 'Caractères';
$lang['ma_none']				= 'Message global';
//###########################################################//


//---------------	BASE DE DATOS - DATA BASES	------------------------//
$lang['od_not_opt']			= 'Erreur !';
$lang['od_not_check']		= 'Erreurs !';
$lang['od_opt']				= 'Optimisé !';
$lang['od_rep']				= 'Réparer !';
$lang['od_check_ok']		= 'Très bien !';
$lang['od_opt_db']			= 'Base de données';
$lang['od_optimize']		= 'Optimiser';
$lang['od_repair']			= 'Réparer';
$lang['od_check']			= 'Contrôler';
$lang['od_select_action']	= 'Sélectionner';
//###########################################################//


//---------------	PÁGINA DE INICIO - OVERVIEW	------------------------//
$lang['ow_title']				= 'Bienvenue sur XG Project';
$lang['ow_welcome_text']		= 'Merci d\'avoir choisi XG Project. Jour après jour, nous travaillons pour devenir le OGame Nº1 OpenSource sur internet. Voici un "aperçu" de l\'ensemble du jeu. Vous pouvez faire toutes les modifications que vous souhaitez en parcourant le menu à gauche.';
$lang['ow_overview']			= 'Panneau de contrôle';
$lang['ow_support']				= 'Support';
$lang['ow_credits']				= 'Crédits';
$lang['ow_forum']				= 'Forum';
$lang['ow_project_leader']		= 'Chef du projet';
$lang['ow_principal_contributors']	= 'Principaux contributeurs';
$lang['ow_special_thanks']			= 'Remerciements spéciaux';
$lang['ow_install_file_detected']	= 'Le dossier install/ a été trouvé. Pour des raisons de sécurité, nous vous recommandons de le supprimer ou de le renommer.';
$lang['ow_config_file_writable']	= 'Le fichier config.php peut être écrit, il est conseillé de le mettre en chmod 440 (lecture seule).';
$lang['ow_database_errors']			= 'Il y a des erreurs dans votre base de données. Vous pouvez les voir en <a href="errors.php">cliquant ici</a>';
$lang['ow_old_version']				= 'Il y a une nouvelle version disponible. Cliquer <a href="http://www.xgproyect.net/downloads.php" target="_blanck">ici</a> pour la télécharger.';
$lang['ow_none']					= 'Pas d\'erreurs';
//###########################################################//


//---------------	CONFIGURACIÓN DEL JUEGO - GAME CONFIG - CONFIGURATION DU JEU	------------------------//
$lang['se_server_parameters']				= 'Paramètres du serveur';
$lang['se_name']							= 'Nom';
$lang['se_lang']							= 'Language';
$lang['se_general_speed']					= 'Rate générale';
$lang['se_normal_speed']					= 'Vitesse de jeu normale: 1 <br> Vitesse maximum recommendée: 5';
$lang['se_normal_speed_resoruces']			= 'Vitesse de production normale: 1 <br> Vitesse maximum recommendée: 5';
$lang['se_normal_speed_fleett']				= 'Vitesse de flottes normale: 1 <br> Vitesse maximum recommendée: 5';
$lang['se_fleet_speed']						= 'Vitesse des flottes';
$lang['se_resources_producion_speed']		= 'Vitesse de production';
$lang['se_forum_link']						= 'Lien du forum';
$lang['se_server_op_close']					= 'Serveur en ligne?';
$lang['se_server_status_message']			= 'Message hors ligne';
$lang['se_server_planet_parameters']		= 'Paramètres de nouvelles planètes';
$lang['se_initial_fields']					= 'Cases initiales';
$lang['se_fields']							= 'Cases';
$lang['se_per_hour']						= 'Par heure';
$lang['se_metal_production']				= 'Production de métal';
$lang['se_crystal_production']				= 'Production de cristal';
$lang['se_deuterium_production']			= 'Production de deutérium';
$lang['se_energy_production']				= 'Production d\'énergie';
$lang['se_several_parameters']				= 'Plusieurs paramètres';
$lang['se_title_admins_protection']			= 'Cochez la case pour activer la protection et empêcher les administrateurs et les modérateurs de recevoir des attaques ou toute autre action de flottes.';
$lang['se_admin_protection']				= 'Protection';
$lang['se_debug_mode']						= 'Mode Debug';
$lang['se_save_parameters']					= 'Enregistrer les paramètres';
$lang['se_configuration_title']				= 'Configuration';
$lang['se_server_naame']					= 'Nom de votre jeu';
$lang['se_cookie_name']						= 'Nom des cookies';
$lang['se_cookie_advert']					= 'Lorsque vous renommez le cookie pour vous et vos joueurs doivent se reconnecter. <br> Recommandation: renommer seulement si vous possédez plusieurs univers.';
$lang['se_debug_message']					= 'Le mode de débug affiche un log de requêtes effectuées au moment';
$lang['se_def_cdr']							= 'Défenses en débris';
$lang['se_ships_cdr']						= 'Vaisseaux en débris';
$lang['se_def_cdr_message']					= 'Définit le pourcentage de débris générés par les défenses après une attaque';
$lang['se_ships_cdr_message']				= 'Définit le pourcentage de débris générés par les vaisseaux après une attaque';
$lang['se_noob_protect']					= 'Protection des novices';
$lang['se_noob_protect2']					= 'Protection N. points';
$lang['se_noob_protect3']					= 'Protection N. limite de points';
$lang['se_noob_protect_e2']					= 'Point limite pour cesser d\'être un novice';
$lang['se_noob_protect_e3']					= 'Cliquer ici pour voir plus d\'infos';
//###########################################################//


//---------------	MODERACIÓN - MODERATION - MODÉRATION	------------------------//
//	PERMISOS - PERMISSIONS
$lang['mod_title']				= 'Droits Admin';
$lang['mod_range']				= 'Rang';
$lang['mod_power_view']			= 'Observation';
$lang['mod_power_edit']			= 'Éditer les droits';
$lang['mod_power_config']		= 'Configurer le jeu';
$lang['mod_power_tools']		= 'Manipuler les outils';
$lang['mod_power_loog']			= 'Actions dans la partie administration sauvegardées dans les logs';

//	NIVELES DE AUTORIDAD - AUTHLEVELS
$lang['ad_authlevel_title']		= 'Rangs';
$lang['ad_authlevel_name']		= 'Pseudo';
$lang['ad_authlevel_auth']		= 'Rang';
$lang['ad_authlevel_succes']	= 'Statut changé avec succès';
$lang['ad_authlevel_error']		= 'Vous possédez déjà ce rang !';
$lang['ad_authlevel_error_2']	= 'Entrer un nom de compte seulement !';
$lang['ad_authlevel_error_3']	= 'Vous ne pouvez pas modifier le rang du créateur du jeu !';
$lang['ad_authlevel_insert_id']	= 'Entrer ID manuellement (Facultatif)';
$lang['ad_authlevel_aa']		= '[Voir les administrateurs]';
$lang['ad_authlevel_oo']		= '[Voir les opérateurs]';
$lang['ad_authlevel_mm']		= '[Voir les modérateurs]';
$lang['ad_authlevel_jj']		= '[Voir les joueurs]';
$lang['ad_authlevel_tt']		= '[Voir tout]';
//###########################################################//


//---------------	FLOTAS EN VUELO - FLYING FLEETS - FLOTTES EN VOL	------------------------//
$lang['ff_flying_fleets']		= 'Flottes en vol';
$lang['ff_ammount']				= 'Quantité';
$lang['ff_mission']				= 'Mission';
$lang['ff_beginning']			= 'Départ';
$lang['ff_departure']			= 'De';
$lang['ff_departure_hour']		= 'Heure de départ';
$lang['ff_objective']			= 'Destination';
$lang['ff_arrival']				= 'Objectif';
$lang['ff_hold_position']		= 'Stationnement allié';
$lang['ff_arrival_hour']		= 'Heure d\'arrivée';
$lang['ff_go_back_now']			= 'Retour';
$lang['ff_delete_succes']		= 'Vol supprimé avec succès';
$lang['ff_goback_succes']		= 'Le vol est en train de retourner à sa planète d\'origine';
//###########################################################//


//---------------	ACTUALIZACIÓN DE ESTADISTICAS - UPDATE STATS - MISE À JOUR DES STATS	------------------------//
$lang['sb_top_memory']			= 'Mémoire maximale: %p KB / max. %m KB <br>';
$lang['sb_final_memory']		= 'Mémoire consommée à la fin: %e KB / max. %m KB<br>';
$lang['sb_start_memory']		= 'Mémoire consommée au début: %i KB / max. %m KB<br>';
$lang['sb_stats_update']		= 'Statistiques mise à jour: %t secondes<br>';
$lang['sb_users_per_block']		= 'Nombre de joueurs par bloc: %n<br>';
$lang['sb_using_fleet_array']	= 'Utilisation d\'array de flottes en vol<br>';
$lang['sb_using_fleet_query']	= 'En utilisant une requête à la base de<br> données pour la flotte du joueur<br>';
$lang['sb_stats_updated']		= 'Mise à jour des statistiques. <br>Résumer:<br>';
//###########################################################//


//---------------	LISTA DE USUARIOS - USERS LIST - LISTE DE JOUEURS	------------------------//
$lang['ul_sure_you_want_dlte']	= 'Êtes-vous sûr de vouloir le supprimer?';
//###########################################################//


//---------------	MENU	------------------------//
$lang['mu_general']					= 'Général';
$lang['mu_connected']				= 'Liste des connectés';
$lang['mu_settings']				= 'Configuration';
$lang['mu_global_message']			= 'Message Global';
$lang['mu_users_settings']			= 'Menu Édition';
$lang['mu_add_delete_resources']	= 'Éditeur de comptes';
$lang['mu_stats_options']			= 'Réglages des statistiques';
$lang['mu_manage_users']			= 'Gérer les joueurs';
$lang['mu_ban_options']				= 'Réglages des bannissements';
$lang['mu_moon_options']			= 'Réglages des lunes';
$lang['mu_observation']				= 'Observation';
$lang['mu_flying_fleets']			= 'Flottes en vol';
$lang['mu_user_list']				= 'Liste des joueurs';
$lang['mu_moon_list']				= 'Liste des lunes';
$lang['mu_mess_list']				= 'Liste des message';
$lang['mu_planet_list']				= 'Liste des planètes';
$lang['mu_error_list']				= 'Erreurs BDD';
$lang['mu_active_planets']			= 'Planètes actives';
$lang['mu_tools']					= 'Outils';
$lang['mu_md5_encripter']			= 'Crypteur MD5';
$lang['mu_optimize_db']				= 'Base de données';
$lang['mu_manual_points_update']	= 'Mise à jour manuel des points';
$lang['mu_mpu_confirmation']		= 'La mise à jour des points est automatique, mais vous pouvez la faire manuellement si vous le souhaitez en cliquant sur le bouton Continuer...';
$lang['mu_search_page']				= 'Recherches avancées';
$lang['mu_info_account_page']		= 'Information compte';
$lang['mu_planets_options']			= 'Réglages des planètes';
$lang['mu_user_logs']				= 'Log';
//###########################################################//


//---------------	REINICIO DE UNIVERSO - RESET UNIVERSE - REMETTRE À ZÉRO DE L'UNIVERS	------------------------//
$lang['re_reset_universe']					= 'Remise à zéro de l\'univers';
$lang['re_reset_universe_confirmation']		= 'En cliquant sur OK, reprendre les options demandés à zéro. Cette opération ne peut pas être inversée, et il est de votre responsabilité de faire une sauvegarde. Si vous remettez à zéro l\'univers tout entier, les comptes ne seront pas supprimés.';
$lang['re_defenses_and_ships']				= 'Hangar de défenses';
$lang['re_defenses']						= 'Remettre à zéro les défenses';
$lang['re_ships']							= 'Remettre à zéro les vaisseaux';
$lang['re_reset_hangar']					= 'Remettre à zéro la file d\'attente(queue) des hangars et défenses';
$lang['re_buldings']						= 'Bâtiments';
$lang['re_buildings_pl']					= 'Remettre à zéro les batîments planètaires';
$lang['re_buildings_lu']					= 'Remettre à zéro les bâtiments lunaires';
$lang['re_reset_buldings']					= 'Remettre à zéro la file d\'attente(queue) des bâtiments';
$lang['re_inve_ofis']						= 'Recherches et officiers';
$lang['re_ofici']							= 'Remettre à zéro les officiers';
$lang['re_investigations']					= 'Remettre à zéro les recherches';
$lang['re_reset_invest']					= 'Remettre à zéro la file d\'attente(queue) de recherches';
$lang['re_resources']						= 'Ressources';
$lang['re_resources_dark']					= 'Remettre à zéro l\'anti-matière';
$lang['re_resources_met_cry']				= 'Remettre à zéro le métal, cristal et deutérium';
$lang['re_general']							= 'Général';
$lang['re_reset_moons']						= 'Remettre à zéro les lunes';
$lang['re_reset_notes']						= 'Remettre à zéro les notes';
$lang['re_reset_rw']						= 'Remettre à zéro les rapports de bataille';
$lang['re_reset_buddies']					= 'Remettre à zéro la liste d\'amis';
$lang['re_reset_allys']						= 'Remettre à zéro les alliances';
$lang['re_reset_fleets']					= 'Remettre à zéro les flottes';
$lang['re_reset_errors']					= 'Remettre à zéro la liste d\'erreurs';
$lang['re_reset_banned']					= 'Remettre à zéro les bannis';
$lang['re_reset_messages']					= 'Remettre à zéro les messages';
$lang['re_reset_statpoints']				= 'Remettre à zéro les statistiques';
$lang['re_reset_all']						= 'Remettre à zéro TOUT l\'univers';
$lang['re_reset_h1']						= 'Remettre à zéro les options';
$lang['re_reset_excess']					= 'Options remis à zéro avec succès';
//###########################################################//


//---------------	INFORMACIÓN DE CUENTAS - DATA ACCOUNTS - DONNÉES DES COMPTES	------------------------//
$lang['ac_user_id_required']				= 'Vous devez sélectionner n\'importe quel joueur !';
$lang['ac_select_one_id']					= 'Veuillez choisir un seul joueur !';
$lang['ac_no_character']					= 'Nous n\'acceptons pas les lettres, seulement les chiffres !';
$lang['ac_select_id_num']					= 'Écrire l\'ID manuellement (Facultatif)';
$lang['ac_select_username']					= 'Sélectionner le joueur';
$lang['ac_username_doesnt']					= 'Ce joueur n\'existe pas !';
$lang['ac_title']							= 'Détails du compte';
$lang['ac_title_entry']						= 'Détaillée ';
$lang['ac_more']							= '(Plus)';
$lang['ac_no_ally']							= 'Pas d\'alliance';
$lang['ac_moon']							= 'Lune';
$lang['ac_enter_user_id']					= 'Sélectionner le joueur';
$lang['ac_minimize_maximize']				= 'Réduire/Agrandir';
$lang['ac_account_data']					= 'Données du compte';
$lang['ac_name']							= 'Nom';
$lang['ac_checkip_title']					= 'Vérification IP';
$lang['ac_checkip'][1]						= '<font color=aqua>Activé</font>';
$lang['ac_checkip'][0]						= '<font color=#FF6600>Désactivé</font>';
$lang['ac_mail']							= 'E-mail';
$lang['ac_perm_mail']						= 'E-mail Permanent';
$lang['ac_auth_level']						= 'Rang';
$lang['ac_on_vacation']						= 'En vacances?';
$lang['ac_banned']							= 'Banni?';
$lang['ac_alliance']						= 'Alliance';
$lang['ac_reg_ip']							= 'IP d\'enregistrement';
$lang['ac_last_ip']							= 'Dernière IP';
$lang['ac_home_planet_id']					= 'ID Planète Mère';
$lang['ac_home_planet_coord']				= 'Coordonnées de la planète mère';
$lang['ac_user_system']						= 'Système du joueur';
$lang['ac_detailed_planet_moon_info']		= 'Détails des planètes et lunes';
$lang['ac_id_names_coords']					= 'ID, Nom et Coordonnées';
$lang['ac_coords']							= 'Coordonnées';
$lang['ac_diameter']						= 'Diamètre';
$lang['ac_fields']							= 'Cases';
$lang['ac_temperature']						= 'Température';
$lang['ac_officier_research']				= 'Recherches et Officiers';
$lang['ac_recent_destroyed_planets']		= 'Planètes détruites récemment';
$lang['ac_no_moons']						= 'Le joueur ne possède pas de lunes';
$lang['ac_total_points']					= 'Total: ';
$lang['ac_points_count']					= 'Score | Total';
$lang['ac_ranking']							= 'Classement';
$lang['ac_see_ranking']						= 'Voir Statistiques';
$lang['ac_ally_ranking']					= 'Statistiques de l\'alliance';
$lang['ac_user_ranking']					= 'Statistiques du joueur';
$lang['ac_register_time']					= 'Enregistré';
$lang['ac_register_ally_time']				= 'Date de création';
$lang['ac_act_time']						= 'Date de la dernière activité';
$lang['ac_info_ally']						= 'Info alliance ';
$lang['ac_leader']							= 'Chef';
$lang['ac_tag']								= 'TAG';
$lang['ac_name_ali']						= 'Nom complet';
$lang['ac_ext_text']						= 'Texte externe';
$lang['ac_int_text']						= 'Texte interne';
$lang['ac_sol_text']						= 'Texte de candidature';
$lang['ac_image']							= 'Log de l\'alliance';
$lang['ac_ally_web']						= 'Site web de l\'alliance';
$lang['ac_total_members']					= 'Nombre de membres';
$lang['ac_no_text_ext']						= 'L\'alliance n\'a pas de texte externe';
$lang['ac_no_text_int']						= 'L\'alliance n\'a pas de texte interne';
$lang['ac_no_text_sol']						= 'L\'alliance n\'a pas de texte de candidature';
$lang['ac_no_img']							= 'L\'alliance n\'a pas de logo';
$lang['ac_no_web']							= 'L\'alliance n\'a pas de site web';
$lang['ac_view_image']						= 'Voir l\'image dans une autre fenêtre';
$lang['ac_view_image2']						= 'Voir l\'image';
$lang['ac_view_text_ext']					= 'Voir texte externe';
$lang['ac_view_text_int']					= 'Voir texte interne';
$lang['ac_view_text_sol']					= 'Voir le texte de candidature';
$lang['ac_moons_no']						= '<font color=red>(Pas de lune)</font>';
$lang['ac_urlnow']							= 'URL: ';
$lang['ac_time_destruyed']					= 'Date de la destruction totale';
$lang['ac_isnodestruyed']					= '<font color=red>(Il n\'y a pas de planètes détruites récemment)</font>';
$lang['ac_no_alliance']						= '<font color=red>(Pas d\'alliance)</font>';
$lang['ac_ali_text_11']						= 'Texte externe';
$lang['ac_ali_text_22']						= 'Texte interne';
$lang['ac_ali_text_33']						= 'Texte de candidature';
$lang['ac_ali_logo_11']						= 'Log de l\'alliance';
$lang['ac_ali_idid']						= 'ID:';
$lang['ac_suspended_title']					= 'Visualiser les bannissements';
$lang['ac_suspended_time']					= 'Banni';
$lang['ac_suspended_longer']				= 'Jusqu\'à';
$lang['ac_suspended_reason']				= 'Raison';
$lang['ac_suspended_autor']					= 'Banni pour';
$lang['ac_note_k']							.= '<table><tr><th width=10%><font color=lime>T+</font></th><th>Dépasse le trillion</th></tr>';
$lang['ac_note_k']							.= '<tr><th width=10%><font color=lime>T</font></th><th>Dépasse ou le même que le trillion</th></tr>';
$lang['ac_note_k']							.= '<tr><th width=10%><font color=lime>B</font></th><th>Dépasse ou le même que le billion</th></tr>';
$lang['ac_note_k']							.= '<tr><th width=10%><font color=lime>M</font></th><th>Dépasse ou le même que le million</th></tr>';
$lang['ac_note_k']							.= '<tr><th width=10%><font color=lime>K</font></th><th>Dépasse ou le même que le millier</th></tr></table>';
$lang['ac_leyend']							= '[ Légende ]';
$lang['ac_no_rank_level']					= 'Vous n\'avez pas les permissions nécessaires pour afficher des informations sur ce joueur !';
//###########################################################//


//---------------	BUSCADOR AVANZADO - ADVANCED SEARCH	------------------------//
$lang['se_no_data']		=	"Il n'y a pas de données.";
$lang['se_intro']		=	'Mot ou mot clé';
$lang['se_users']		=	'Joueurs';
$lang['se_planets']		=	'Planètes';
$lang['se_moons']		=	'Lunes';
$lang['se_allys']		=	'Alliances';
$lang['se_suspended']	=	'Banni';
$lang['se_vacations']	=	'Vacances';
$lang['se_authlevels']	=	'Administrateurs';
$lang['se_inactives']	=	'Inactifs';
$lang['se_planets_act']	=	'Planètes actives';
$lang['se_type_typee']	=	'Type';
$lang['se_type_all']	=	'Recherche normale';
$lang['se_type_exact']	=	'Recherche exacte';
$lang['se_type_last']	=	'Commencer par un mot ou une lettre';
$lang['se_type_first']	=	'Fin avec n\'importe quel mot ou une lettre';
$lang['se_search']		=	'Rechercher';
$lang['se_name']		=	'Nom';
$lang['se_id_owner']	=	'ID du propriétaire';
$lang['se_galaxy']		=	'Galaxie';
$lang['se_system']		=	'Système';
$lang['se_planet']		=	'Planète';
$lang['se_tag']			=	'TAG';
$lang['se_email']		=	'E-mail';
$lang['se_auth']		=	'Rang';
$lang['se_activity']	=	'Dernière activité';
$lang['se_ban']			=	'Banni';
$lang['se_vacat']		=	'Vacances';
$lang['se_ban_reason']	=	'Raison';
$lang['se_ban_time']	=	'Banni';
$lang['se_ban_limit']	=	'Jusqu\'à';
$lang['se_ban_author']	=	'Banni pour';
$lang['se_search_title']	=	'Recherches avancées';
$lang['se_order_list_now']	=	'Ordre de recherche';
$lang['se_input_name']		=	'Nom';
$lang['se_input_g']			=	'Galaxie';
$lang['se_input_s']			=	'Système';
$lang['se_input_p']			=	'Planète';
$lang['se_input_inacti']	=	'Inactivité';
$lang['se_input_submit']	=	'Ordre';
$lang['se_input_authlevel']	=	'Permissions';
$lang['se_input_email']		=	'E-mail';
$lang['se_input_time']		=	'Temps';
$lang['se_input_longer']	=	'Durée';
$lang['se_input_authlevel']	=	'Permissions';
$lang['se_input_prop']		=	'Propriétaire';
$lang['se_input_asc']		=	'Croissant';
$lang['se_input_desc']		=	'Décroissant';
$lang['se_input_activity']	=	'Activité';
$lang['se_input_register']	=	'Enregistrement';
$lang['se_input_members']	=	'Nombre de membres';
$lang['se_input_members2']	=	'Membres';
$lang['se_input_have_moon']	=	'Possède une lune?';
$lang['se_input_ip']		=	'IP';
$lang['se_input_a']			=	'A';
$lang['se_input_d']			=	'D';
$lang['se_input_hay']		=	'Il y a ';
$lang['se_input_inact']		=	' inactif/s';
$lang['se_input_susss']		=	' banni';
$lang['se_input_admm']		=	' administrateur/s';
$lang['se_input_allyy']		=	' alliance/s';
$lang['se_input_planett']	=	' planète/s';
$lang['se_input_moonn']		=	' lune/s';
$lang['se_input_userss']	=	' joueur/s';
$lang['se_input_vacatii']	=	' joueur/s en vacances';
$lang['se_input_connect']	=	' joueur/s connecté/s';
$lang['se_input_act_pla']	=	' planète/s active/s';
$lang['se_filter_title']	=	'Filtre';
$lang['se_search_in']		=	'Pour';
$lang['online_users']		=	'Connecté';
$lang['se_limit']			=	'Limite';
$lang['se_pagees']			=	'Pages';
$lang['se_time_of_page']	=	'Page créée en ';
$lang['se_expand']			=	'Agrandir';
$lang['se_contrac']			=	'Réduire';
$lang['se__next']			=	'Suivant';
$lang['se__before']			=	'Retour';
$lang['se_search_info']		=	'Consulter';
$lang['se_asc_desc']		=	'CROISSANT / DÉCROISSANT';
$lang['se_search_order']	=	'Ordre';
$lang['se_search_edit']		=	'Éditer';
$lang['se_delete_succes_p']	=	'Supprimé avec succès';
$lang['se_confirm_planet']	=	'La planète sélectionnée sera effacée complètement, soit une planète mère ou une colonie, voulez-vous continuer? Nom de la planète:';

$SE_ID		=	"ID";
$SE_NAME	=	"Nom";

// BÚSQUEDA EN TABLA DE USUARIOS
$lang['se_search_users'][0]	=	$SE_ID;
$lang['se_search_users'][1]	=	$SE_NAME;
$lang['se_search_users'][2]	=	'E-mail';
$lang['se_search_users'][3]	=	'Dernière activité';
$lang['se_search_users'][4]	=	'Date d\'enregistrement';
$lang['se_search_users'][5]	=	'Dernière IP';
$lang['se_search_users'][6]	=	'Permissions';
$lang['se_search_users'][7]	=	'Banni ?';
$lang['se_search_users'][8]	=	'Mode Vacances ?';

// BÚSQUEDA EN TABLA DE PLANETAS
$lang['se_search_planets'][0]	=	$SE_ID;
$lang['se_search_planets'][1]	=	$SE_NAME;
$lang['se_search_planets'][2]	=	"Propriétaire";
$lang['se_search_planets'][3]	=	"Dernière activité";
$lang['se_search_planets'][4]	=	"Galaxie";
$lang['se_search_planets'][5]	=	"Système";
$lang['se_search_planets'][6]	=	"Planète";

// BÚSQUEDA EN TABLA DE SUSPENDIDOS
$lang['se_search_banned'][0]	=	$SE_ID;
$lang['se_search_banned'][1]	=	$SE_NAME;
$lang['se_search_banned'][2]	=	"Date de bannissement";
$lang['se_search_banned'][3]	=	"Date de fin de bannissement";
$lang['se_search_banned'][4]	=	"Raison";
$lang['se_search_banned'][5]	=	"Auteur";

// BÚSQUEDA EN TABLA DE LA ALIANZA
$lang['se_search_alliance'][0]	=	$SE_ID;
$lang['se_search_alliance'][1]	=	$SE_NAME;
$lang['se_search_alliance'][2]	=	"TAG";
$lang['se_search_alliance'][3]	=	"Chef";
$lang['se_search_alliance'][4]	=	"Date de création";
$lang['se_search_alliance'][5]	=	"Nombre de membres";
//###########################################################//



//---------------	CREADOR - MAKER	------------------------//
$lang['new_creator_title']		= 'Créateur';
$lang['new_creator_title_u']	= 'Créer/Supprimer joueurs';
$lang['new_creator_title_p']	= 'Créer/Supprimer planètes';
$lang['new_creator_title_l']	= 'Créer/Supprimer lunes';
$lang['new_creator_coor']		= 'Coordonnées';
$lang['new_creator_go_back']	= '[ Retourner au menu ]';
$lang['new_creator_refresh']	= '[ Actualiser ]';

//	CREADOR DE USUARIOS - USERS MAKER
$lang['new_title']				= 'Ajout nouveau joueur';
$lang['new_only_numbers']		= 'Les coordonnées doivent être uniquement des chiffres !';
$lang['new_error_coord']		= "Coordonnées ne correspondent pas !";
$lang['new_complete_all']		= 'Insérer toutes les données';
$lang['new_error_name']			= 'Le nom du joueur est déjà utilisé !';
$lang['new_error_email']		= 'L\'adresse e-mail est déjà utilisée !';
$lang['new_error_email2']		= 'L\'adresse e-mail n\'est pas valide !';
$lang['new_error_passw']		= 'Le mot de passe doit contenir au moins 4 caractères !';
$lang['new_error_galaxy']		= 'Les coordonnées spécifiées sont déjà en cours d\'utilisation !';
$lang['new_user_success']		= 'Joueur créé avec succès !';
$lang['new_add_user']			= 'Ajouter un nouveau joueur';
$lang['new_range']				= 'Rang';
$lang['new_coord']				= 'Coordonnées';
$lang['new_email']				= 'E-mail';
$lang['new_pass']				= 'Mot de passe';
$lang['new_name']				= 'Nom';

//	CREADOR DE PLANETAS - PLANETS MAKER
$lang['po_galaxy']				=	'Galaxie';
$lang['po_system']				=	'Système solaire';
$lang['po_planet']				=	'Planète';
$lang['po_colony']				=	'Colonie';
$lang['po_fields_max']			=	'Cases';
$lang['po_coor_1']				=	'Coordonnées';
$lang['po_add_planet']			=	'Ajouter planète';
$lang['po_delete_planet']		=	'Supprimer planète';
$lang['po_name_planet']			=	'Nom de la planète';
$lang['po_complete_all']		=	'Données invalides ou coordonnées déjà en cours d\'utilisation !';
$lang['po_complete_all2']		=	'Erreur sur les coordonnées !';
$lang['po_complete_succes']		=	'Planète créée avec succès ! | <a href="AccountEditorPage.php">Éditer la planète</a>';
$lang['po_complete_invalid']	=	'Des données invalides !';
$lang['po_complete_invalid2']	=	'La planète n\'existe pas !';
$lang['po_complete_invalid3']	=	'Les planètes seulement peuvent être supprimées !';
$lang['po_complete_succes2']	=	'Planète supprimée avec succès !';

//	CREADOR DE LUNAS - MOON MAKER
$lang['mo_moon_added']				= 'Lune ajoutée avec succès !';
$lang['mo_moon_deleted']			= 'Lune supprimée avec succès !';
$lang['mo_moon_doesnt_exist']		= 'La lune n\'existe pas !';
$lang['mo_moon_only']				= 'Vous ne pouvez supprimer que les lunes !';
$lang['mo_moon_unavaible']			= 'Cet planète possède déjà une lune !';
$lang['mo_planet_doesnt_exist']		= 'La planète n\'existe pas !';
$lang['mo_moon_name']				= 'Nom de la lune';
$lang['mo_moon']					= 'Lune';
$lang['mo_diameter']				= 'Diamètre';
$lang['mo_temperature']				= 'Température';
$lang['mo_fields_avaibles']			= 'Cases disponibles';
$lang['po_add_moon']				= 'Ajouter lune';
$lang['po_delete_moon']				= 'Supprimer lune';
//###########################################################//


//---------------	HISTORIAL - LOG	------------------------//
$lang['log_viewmod'][0] 			= 'Non';
$lang['log_viewmod'][1] 			= 'Oui';
$lang['log_log_body_build']			= 'Log bâtiments';
$lang['log_log_body_resou']			= 'Log ressources';
$lang['log_log_body_def']			= 'Log défenses';
$lang['log_log_body_ships']			= 'Log vaisseaux';
$lang['log_log_body_techs']			= 'Log recherches';
$lang['log_log_body_offi']			= 'Log officiers';
$lang['log_log_body_personal']		= 'Log données personnelles';
$lang['log_log_body_ally']			= 'Log alliance';
$lang['log_log_body_p_m']			= 'Log planètes et lunes';
$lang['log_log_body_reset']			= 'Log remise à zéro du système';
$lang['log_log_body_mod']			= 'Log système restreint';
$lang['log_log_body_gen']			= 'Log général';
$lang['log_log_body_config']		= 'Log configuration';
$lang['log_log_title_22']			= 'Log';
$lang['log_alert']					= 'En cliquant sur accepter vous souhaitez supprimer tout le contenu du fichier texte, voulez-vous continuer?';
$lang['log_input_value']			= 'Éditer';
$lang['log_go_back']				= '[&nbsp; Retour &nbsp;]';
$lang['log_edit_succes']			= 'Edité avec succès';
$lang['log_delete_succes']			= 'Le fichier sélectionné a été vidé avec succès:&nbsp;';
$lang['log_edit_link']				= '[&nbsp; Éditer l\'historique &nbsp;]';
$lang['log_delete_link']			= '[&nbsp; Vider l\'historique &nbsp;]';
$lang['log_filesize_0']				= '<font color=yellow>L\'historique est vide.</font>';
$lang['log_the_user']				= 'L\'utilisateur ';
$lang['log_to_planet']				= 'de la planète avec l\'ID: ';
$lang['log_and']					= 'et ';
$lang['log_to_user']				= 'du joueur avec l\'ID: ';
$lang['log_moree']					= 'ajouter';
$lang['log_nomoree']				= 'supprimer';
$lang['log_modify_personal']		= 'modifier les données suivantes';
$lang['log_title']					= 'Panel d\'administration des logs';
$lang['log_name_of_ally']			= 'Nom de l\'alliance';
$lang['log_tag_of_ally']			= 'TAG de l\'alliance';
$lang['log_idnewleader']			= 'ID du nouveau leader';
$lang['log_text_ext']				= 'Texte externe';
$lang['log_text_int']				= 'Texte interne';
$lang['log_text_sol']				= 'Texte de candidature';
$lang['log_ally_delete']			= 'L\'ALLIANCE A ÉTÉ SUPPRIMÉE';
$lang['log_id_user_expu']			= 'ID du joueur qui a été expulsé de l\'alliance';
$lang['log_to_ally_whosid']			= 'à l\'alliance avec l\'ID: ';
$lang['log_change_name_pla']		= 'Changer le nom de';
$lang['log_delete_all_edi']			= 'Supprimer tous les bâtiments';
$lang['log_delete_all_ships']		= 'Supprimer tous les vaisseaux';
$lang['log_delete_all_def']			= 'Supprimer toutes les défenses';
$lang['log_delete_all_c_han']		= 'Supprimer la liste d\'attente du hangar';
$lang['log_delete_all_c_edi']		= 'Supprimer la file des bâtiments';
$lang['log_change_diameter']		= 'Changer le diamètre par';
$lang['log_change_fields']			= 'Changer le nombre de cases par';
$lang['log_change_pla_pos']			= 'Changer la position ';
$lang['log_planet_pos']				= 'de la planète';
$lang['log_moon_pos']				= 'de la lune';
$lang['log_reseteo']				= 'remise à zéro';
$lang['log_defenses']				= 'DÉFENSES';
$lang['log_ships']					= 'VAISSEAUX';
$lang['log_c_hangar']				= 'FILES D\'ATTENTE HANGARS';
$lang['log_buildings_planet']		= 'BÂTIMENTS PLANÈTES';
$lang['log_buildings_moon']			= 'BÂTIMENTS LUNES';
$lang['log_c_buildings']			= 'FILE DES BÂTIMENTS';
$lang['log_researchs']				= 'RECHERCHES';
$lang['log_officiers']				= 'OFFICIERS';
$lang['log_c_researchs']			= 'RECHERCHES EN COURS';
$lang['log_darkmatter']				= 'ANTI-MATIÈRE';
$lang['log_resources']				= 'RESSOURCES';
$lang['log_notes']					= 'NOTES';
$lang['log_rw']						= 'RAPPORTS DE COMBAT';
$lang['log_friends']				= 'LISTE D\'AMIS';
$lang['log_alliances']				= 'ALLIANCES';
$lang['log_fleets']					= 'FLOTTES EN VOL';
$lang['log_errors']					= 'LISTE DES ERREURS';
$lang['log_banneds']				= 'JOUEURS BANNIS';
$lang['log_messages']				= 'MESSAGES';
$lang['log_statpoints']				= 'STATISTIQUES';
$lang['log_moons']					= 'LUNES';
$lang['log_all_uni']				= 'TOUS LES UNIVERS';
$lang['log_change_auth_1']			= 'Modifier les permissions du joueur avec l\'ID ';
$lang['log_change_auth_2']			= 'Les permissions correctes maintenant sont: ';
$lang['log_can_view_mod']			= 'Les modérateurs peuvent voir... ';
$lang['log_can_view_ope']			= 'Les opérateurs peuvent voir... ';
$lang['log_tools']					= 'Outils?';
$lang['log_edit']					= 'Éditer?';
$lang['log_view']					= 'Voir le menu d\'observation?';
$lang['log_config']					= 'Configurer les options de jeu?';
$lang['log_system_mod_title']		= '<font color=lime>||| MODÉRATION SYSTÈME |||</font>';
$lang['log_system_auth_title']		= '<font color=lime>||| CHANGER LE RANG |||</font>';
$lang['log_errors_title']			= '<font color=lime>||| LISTE DES ERREURS |||</font>';
$lang['log_delete_all_errors']		= 'a supprimé tous les messages d\'erreur';
$lang['log_delete_errors']			= 'a supprimé une erreur';
$lang['log_queries_title']			= '<font color=lime>||| CONSULTER SQL |||</font>';
$lang['log_queries_succes']			= 'a implanté la requête SQL suivante:';
$lang['log_suspended_title']		= '<font color=lime>||| SYSTÈME DE BANNISSEMENT |||</font>';
$lang['log_suspended_1']			= 'le joueur a été suspendu ';
$lang['log_suspended_2']			= 'Avec les spécifications suivantes:';
$lang['log_suspended_3']			= 'a supprimé la bannissement ';
$lang['log_reason']					= 'Raison: ';
$lang['log_time']					= 'le: ';
$lang['log_longer']					= 'jusqu\'au: ';
$lang['log_vacations']				= 'Mode vacances? ';
$lang['log_info_detail_title']		= '<font color=lime>||| INFORMATIONS DETAILLÉES |||</font>';
$lang['log_searchto_1']				= ' a recherché des informations sur le joueur ';
$lang['log_attempt_1']				= ' a essayé d\'entrer dans le panneau d\'administration';
$lang['log_new_user']				= ' a créé un nouveau joueur avec les caractéristiques suivantes';
$lang['log_new_user_title']			= '<font color=lime>||| CRÉATION DE JOUEURS |||</font>';
$lang['log_new_user_name']			= 'Nom';
$lang['log_new_user_coor']			= 'Coordonnées';
$lang['log_new_user_auth']			= 'Rang';
$lang['log_new_user_email']			= 'E-mail';
$lang['log_circular_message']		= '<font color=lime>||| MESSAGE CIRCULAIRE |||</font>';
$lang['log_message_specify']		= ' a envoyé un message circulaire avec ces spécifications';
$lang['log_mes_subject']			= 'Sujet';
$lang['log_mes_text']				= 'Message';
$lang['log_operation_succes']		= 'Opération le: ';
$lang['log_database_view']			= ' utilise la base de données qui fait l\'opération suivante ';
$lang['log_database_title']			= '<font color=lime>||| BASE DE DONNÉES |||</font>';
$lang['log_data_optimize']			= 'OPTIMISER';
$lang['log_data_repair']			= 'RÉPARER';
$lang['log_data_check']				= 'CONTRÔLER';
$lang['log_change_stats']			= ' a changé les paramètres des statistiques';
$lang['log_stats_value']			= 'Valeur de 1 point statique';
$lang['log_stats_value_2']			= 'Temps de mise à jour des statistiques (minutes)';
$lang['log_stats_value_3']			= 'Bloquer le joueur';
$lang['log_stats_value_4']			= 'Bloc de mise à jour de la flotte';
$lang['log_stats_value_5']			= 'L\'administration peut être dans le classement?';
$lang['log_stats_value_6']			= 'Jusqu\'à quel place peut être inclus dans le classement?';
$lang['log_sett_no1']				= ' a modifié la configuration suivante';
$lang['log_sett_close']				= 'Serveur fermé? ';
$lang['log_sett_close_rea']			= 'Raison? ';
$lang['log_sett_debug']				= 'Mode DEBUG activé? ';
$lang['log_sett_name_game']			= 'Nom du jeu ';
$lang['log_sett_forum_url']			= 'URL du forum ';
$lang['log_sett_velo_game']			= 'Vitesse du jeu ';
$lang['log_sett_velo_flottes']		= 'Vitesse des flottes ';
$lang['log_sett_velo_prod']			= 'Vitesse de production';
$lang['log_sett_fields']			= 'Cases initiales ';
$lang['log_sett_basic_m']			= 'Métal de base par heure ';
$lang['log_sett_basic_c']			= 'Cristal de base par heure ';
$lang['log_sett_basic_d']			= 'Deutérium de base par heure ';
$lang['log_sett_adm_protection']	= 'Protection contre les administrateurs actifs? ';
$lang['log_sett_language']			= 'Langage ';
$lang['log_sett_debris_def']		= 'Défenses en débris ';
$lang['log_sett_debris_flot']		= 'Vaisseaux en débris ';
$lang['log_sett_act_noobs']			= 'Protection contre nouveaux activée? ';
$lang['log_sett_noob_time']			= 'Temps pour se protéger contre nouveaux ';
$lang['log_sett_noob_multi']		= 'Points limites pour la protection des nouveaux ';
$lang['log_sett_name_cookie']		= 'Nom du cookie ';
$lang['log_searchindb_del1']		= 'Le joueur avec l\'ID ';
$lang['log_searchindb_del3']		= 'La planète avec l\'ID ';
$lang['log_searchindb_del2']		= ' a été éliminée du jeu pour';
$lang['log_searchindb_users']		= 'Joueurs';
$lang['log_searchindb_planets']		= 'Planètes';
$lang['log_searchindb_moons']		= 'Lunes';
$lang['log_searchindb_allys']		= 'Alliances';
$lang['log_searchindb_vacat']		= 'Vacances';
$lang['log_searchindb_susp']		= 'Banni';
$lang['log_searchindb_admin']		= 'Administrateurs';
$lang['log_searchindb_inac']		= 'Inactifs';
$lang['log_search_advert_popup']	= 'Pour activer / désactiver l\'historique faites le à partir de la section Autorisations. Cliquer ici pour aller au panneau de permissions.';
//###########################################################//
?>
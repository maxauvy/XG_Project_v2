<?php

//general
$lang['index']				= 'Index';
$lang['register']			= 'Inscription';
$lang['forum']				= 'Forum';

/* ------------------------------------------------------------------------------------------ */

//index.php
//case lostpassword
$lang['mail_not_exist'] 		= 'L\'adresse e-mail n\'existe pas !';
$lang['mail_title']			= 'Nouveau mot de passe';
$lang['mail_text']			= 'Votre nouveau mot de passe: ';
$lang['mail_sended']			= 'Votre nouveau mot de passe a été envoyé avec succès !';

//case default
$lang['login_error']			= 'Données entrées incorrectes ! <br /><a href="index.php" target="_top">Retour à l\'accueil</a>';

/* ------------------------------------------------------------------------------------------ */

//lostpassword.tpl
$lang['lost_pass_title']		= 'Récupération de mot de passe';
$lang['lost_pass_text'] 		= 'Pour récupérer votre mot de passe, entrez l\'email utilisée à l\'inscription. Vous recevrez un nouveau mot de passe dès que possible.';
$lang['retrieve_pass']			= 'Récupérer mot de passe';
$lang['email']					= 'Adresse e-mail';

//index_body.tpl
$lang['user']				= 'Nom du joueur';
$lang['pass']				= 'Mot de passe';
$lang['remember_pass']			= 'Se rappeler de moi';
$lang['lostpassword']			= 'Mot de passe oublié ?';
$lang['welcome_to']			= 'Bienvenue sur';
$lang['server_description']		= '</strong> est un <strong>jeu de stratégie dans l\'espace</strong> avec <strong>des milliers de joueurs</strong> se combattant partout dans le monde <strong>simultanément</strong> pour devenir le meilleur. Tout ce que vous aurez besoin pour jouer est un navigateur internet.';
$lang['server_register']		= 'S\'enregistrer !';
$lang['server_message']			= 'Inscrivez-vous et découvrez le monde fantastique de';
$lang['login']				= 'Connexion';

/* ------------------------------------------------------------------------------------------ */

//reg.php
$lang['reg_mail_text_part1']		= 'Nous vous remercions de vous être inscrit au jeu. \n Votre mot de passe est: ';
$lang['reg_mail_text_part2']		= ' \n\n Bon amusement ! \n ';
$lang['register_at']			= 'Inscription à ';
$lang['reg_mail_message_pass']		= 'Merci de vous être inscrit !';
$lang['invalid_mail_adress']		= 'L\'adresse e-mail est invalide !<br />';
$lang['empty_user_field']		= 'Entrer un nom de joueur !<br />';
$lang['password_lenght_error']		= 'Le mot de passe doit contenir 4 caractères au minimum !<br />';
$lang['user_field_no_alphanumeric']	= 'Le nom du joueur doit être composé de caractères alphanumériques !<br />';
$lang['terms_and_conditions']		= 'Vous devez accepter les termes et conditions d\'utilisation !<br />';
$lang['user_already_exists']		= 'Ce nom de joueur est déjà utilisé !<br />';
$lang['mail_already_exists']		= 'Cet adresse e-mail est déjà utilisée !<br />';
$lang['welcome_message_from']		= 'Admin';
$lang['welcome_message_sender']		= 'Admin';
$lang['welcome_message_subject']	= 'Bienvenue';
$lang['welcome_message_content']	= 'Bienvenue sur XG Project !<p>Au début, construisez une mine de métal.<br />Pour la faire, cliquez sur le lien "Bâtiments" dans le menu à gauche, et cliquez sur "Construire" à droite de la mine de métal.<br />Patientez un peu, et maintenant votre mine devrait être terminée.<br />Comme vous ne pouvez rien produire sans énergie, vous devez construire une centrale électrique solaire : retournez dans "Bâtiments", et choisissez de construire une centrale électrique solaire.<br />Pour voir tous les vaisseaux, défenses, bâtiments et recherches débloquables, vous pouvez jeter un oeil à l\'arbre technologique dans "Technologie" dans le menu de gauche.<br />Vous pouvez maintenant commencer la conquête de l\'univers... Bonne chance !</p>';
$lang['newpass_smtp_email_error']	= '<br><br>Une erreur s\'est produite lors de l\'envoi du courriel. Votre mot de passe est: ';
$lang['reg_completed']			= 'Inscription terminée !';

//registry_form.tpl
$lang['server_message_reg']		= 'Inscrivez-vous et découvrez le monde fantastique de';
$lang['register_at_reg']		= 'Inscription à';
$lang['user_reg']			= 'Nom du joueur';
$lang['pass_reg']			= 'Mot de passe';
$lang['email_reg']			= 'Adresse e-mail';
$lang['register_now']			= 'S\'enregistrer !';
$lang['accept_terms_and_conditions']	= 'Accepter les termes et conditions d\'utilisation';

?>
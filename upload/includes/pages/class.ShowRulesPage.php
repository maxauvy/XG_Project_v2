<?php

/**
 * @package		XG Project
 * @copyright	Copyright (c) 2008 - 2015
 * @license		http://opensource.org/licenses/gpl-3.0.html	GPL-3.0
 * @since		Version 2.10.0
 */

if(!defined('INSIDE')){ die(header("location:../../"));}

class ShowRulesPage
{
	function __construct ( $CurrentUser )
	{
		global $lang;

		$parse 	= $lang;

		return display ( parsetemplate ( gettemplate ( 'general/rules_body' ) , $parse ) , FALSE );
	}
}
?>

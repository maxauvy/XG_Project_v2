# XG Project v2 #

XG Project est un clone du jeu OGame, créé par Gameforge. XG Project se veut dans la continuité de XG Proyect, créé par lucky (@XGProyect). Le but est simplement de créer un clone totalement libre, gratuit et simple d'utilisation. XG Project est publié sous licence GPL v3, consultable dans le fichier LICENSE présent dans ce dépôt.

## Pré-requis ##

* PHP 5.4.0 ou plus récent
* MySQL 5.0.19 ou plus récent

## Installation et mise-à-jour ##

Si vous installez XG Project pour la première fois, il vous suffit de copier l'intégralité du contenu du dossier "upload" dans un sous-dossier de votre hébergement web et de faire pointer un (sous-)domaine sur ce dossier, puis de suivre les étapes d'installation en vous rendant à l'adresse choisie via votre navigateur web.

Si vous mettez à jour XG Project, vous devez remplacer tous les fichiers présents sur votre hébergement par ceux contenus dans le dossier "upload" de cette archive, à l'exception des fichiers config.php et config.xml, lesquels contiennent respectivement les informations de connexion à votre base de données MySQL et la configuration de votre jeu.



# # XG Project v2

XG Project is a clone of the game OGame, created by Gameforge. XG Project is intended as a continuation of XG Proyect, created by Lucky (XGProyect). The goal is simply to create a totally libre/open-source clone, free and easy to use. XG Project is released under GPL v3, available in the file LICENSE present in this archive.

## Prerequisites ##
* PHP 5.4.0 or newer
* MySQL 5.0.19 or newer

## Installation and update ##

If you install XG Project for the first time, you just have to copy the entire content of the "upload" sub-folder in a folder of your web hosting and point a (sub)domain on this folder, then follow the installation steps by pointing your web browser to the domain you chose.

If you update XG Project, you must replace all the files on your web hosting by those in the "upload" folder of this archive, except for config.php and config.xml files, which respectively contain the connection credentials to your MySQL database and the configuration of your game.
